//��������� ����� � ������� ����� �������� ������ ������������ ����.
//�������������� ���������� �����.
#include <iostream>
#include <vector>
#include <set>

using namespace std;

struct Edge{
    Edge(size_t _second, int _cost): second(_second), cost(_cost){}
    size_t second;
    int cost;
};

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    void AddEdge(size_t first, size_t second, int cost);
    size_t Size() const;
    void GetNeighbours(size_t vertex, vector<Edge>& neighbours) const;
private:
    vector<vector<Edge>> edges;
};

void Graph::AddEdge(size_t first, size_t second, int cost){
    edges[first].emplace_back(second, cost);
    edges[second].emplace_back(first, cost);
}

size_t Graph::Size() const{
    return edges.size();
}

void Graph::GetNeighbours(size_t vertex, vector<Edge>& neighbours) const{
    neighbours = edges[vertex];
}

int FindWeightOfMST(const Graph& g){
    vector<int> slEdges(g.Size(), -1);
    slEdges[0] = 0;
    vector<bool> used(g.Size(), false);
    used[0] = true;
    set<pair<int, size_t>> cont;
    vector<Edge> initNbrs;
    g.GetNeighbours(0, initNbrs);
    for(auto e : initNbrs){
        slEdges[e.second] = e.cost;
        cont.insert(make_pair(e.cost, e.second));
    }
    for(size_t i = 0; i < g.Size() - 1; ++i){
        size_t v = cont.begin()->second;
        slEdges[v] = cont.begin()->first;
        used[v] = true;
        cont.erase(cont.begin());
        vector<Edge> nbrs;
        g.GetNeighbours(v, nbrs);
        for(auto e : nbrs){
            if(used[e.second]){
                continue;
            }
            auto it = cont.find(make_pair(slEdges[e.second], e.second));
            if(it == cont.end()){
                slEdges[e.second] = e.cost;
                cont.insert(make_pair(slEdges[e.second], e.second));
            }
            else{
                if(slEdges[e.second] > e.cost){
                    slEdges[e.second] = e.cost;
                    cont.erase(it);
                    cont.insert(make_pair(e.cost, e.second));
                }
            }
        }
    }
    int s = 0;
    for(auto e : slEdges){
        s += e;
    }
    return s;
}

int main(){
    size_t n, m;
    cin >> n >> m;
    Graph g(n);
    for(size_t i = 0; i < m; ++i){
        size_t b, e;
        cin >> b >> e;
        int w;
        cin >> w;
        g.AddEdge(b - 1, e - 1, w);
    }
    cout << FindWeightOfMST(g);
    return 0;
}
