#include <iostream>
#include <vector>
#include <set>
#include <limits>

using namespace std;

struct Edge{
    Edge(size_t _second, size_t _cost): second(_second), cost(_cost){}
    size_t second;
    size_t cost;
};

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    void AddEdge(size_t first, size_t second, size_t cost);
    size_t Size() const;
    void GetNeighbours(size_t vertex, vector<Edge>& neighbours) const;
private:
    vector<vector<Edge>> edges;
};

void Graph::AddEdge(size_t first, size_t second, size_t cost){
    edges[first].emplace_back(second, cost);
}

size_t Graph::Size() const{
    return edges.size();
}

void Graph::GetNeighbours(size_t vertex, vector<Edge>& neighbours) const{
    neighbours = edges[vertex];
}

size_t FindShortestPath(const Graph& g, size_t start, size_t fin){
    vector<size_t> dist(g.Size(), numeric_limits<size_t>::max());
    dist[start] = 0;
    set<pair<size_t, size_t>> cont;
    cont.emplace(dist[start], start);
    for(size_t i = 0; i < g.Size(); ++i){
        size_t nextV = cont.begin()->second;
        if(nextV == fin){
            return dist[nextV];
        }
        cont.erase(cont.begin());
        vector<Edge> neighbours;
        g.GetNeighbours(nextV, neighbours);
        for(auto edge : neighbours){
            if(dist[nextV] + edge.cost < dist[edge.second]){
                auto it = cont.find(make_pair(dist[edge.second], edge.second));
                if (it != cont.end()){
                    cont.erase(it);
                }
                dist[edge.second] = dist[nextV] + edge.cost;
                cont.emplace(dist[edge.second], edge.second);
            }
        }
    }
}

int main(){
    size_t a, b, m, x, y;
    cin >> a >> b >> m >> x >> y;
    Graph g(m);
    for(size_t i = 0; i < m; ++i){
        g.AddEdge(i, (i + 1) % m, a);
        g.AddEdge(i, (i * i + 1) % m, b);
    }
    cout << FindShortestPath(g, x, y);
    return 0;
}
