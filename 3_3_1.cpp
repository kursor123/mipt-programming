#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <assert.h>
#include <queue>
#include <stack>

using namespace std;

struct Node{
    int key;
    Node* lchild = nullptr;
    Node* rchild = nullptr;
    int height = 1;
    int size = 1;
    int lvl = 0;
    Node(int _key) : key(_key){}
    ~Node(){
        delete lchild;
        delete rchild;
    }
};

class AVL{
    public:
        void insert(int _key);
        void remove(int _key);
        void state();
        int size() const;
        int get_place(int _key) const;
        int get_key(int _place) const;
        AVL() = default;
        ~AVL(){
            delete root;
        }
    private:
        Node* root = nullptr;
        int height(const Node* _node) const;
        void fix_height(Node* _node);
        int size(const Node* _node) const;
        void fix_size(Node* _node);
        void rotate_left(Node* _node);
        void rotate_right(Node* _node);
        void big_rotate_left(Node* _node);
        void big_rotate_right(Node* _node);
        int diff(const Node* _node) const;
        void balance(Node* _node);
};

int AVL::height(const Node* _node) const{
    return (_node != nullptr)?_node->height:0;
}

void AVL::fix_height(Node* _node){
    if(_node == nullptr){
        return;
    }
    _node->height = max(this->height(_node->lchild), this->height(_node->rchild)) + 1;
}

int AVL::size(const Node* _node) const{
    return (_node != nullptr)?_node->size:0;
}

void AVL::fix_size(Node* _node){
    _node->size = this->size(_node->lchild) + this->size(_node->rchild) + 1;
}

void AVL::rotate_left(Node* _node){
    Node* leftSon = _node->lchild;
    Node* rightSon = _node->rchild;
    Node* leftGrandSon = rightSon->lchild;
    Node* rightGrandSon = rightSon->rchild;
    swap(_node->key, rightSon->key);
    _node->lchild = rightSon;
    _node->rchild = rightGrandSon;
    rightSon->lchild = leftSon;
    rightSon->rchild = leftGrandSon;
    this->fix_height(rightSon);
    this->fix_size(rightSon);
    this->fix_height(_node);
    this->fix_size(_node);
    leftSon = nullptr;
    rightSon = nullptr;
    leftGrandSon = nullptr;
    rightGrandSon = nullptr;
}

void AVL::rotate_right(Node* _node){
    Node* rightSon = _node->rchild;
    Node* leftSon = _node->lchild;
    Node* leftGrandSon = leftSon->lchild;
    Node* rightGrandSon = leftSon->rchild;
    swap(_node->key, leftSon->key);
    _node->rchild = leftSon;
    _node->lchild = leftGrandSon;
    leftSon->lchild = rightGrandSon;
    leftSon->rchild = rightSon;
    this->fix_height(leftSon);
    this->fix_size(leftSon);
    this->fix_height(_node);
    this->fix_size(_node);
    leftSon = nullptr;
    rightSon = nullptr;
    leftGrandSon = nullptr;
    rightGrandSon = nullptr;
}

void AVL::big_rotate_left(Node* _node){
    this->rotate_right(_node->rchild);
    this->rotate_left(_node);
}

void AVL::big_rotate_right(Node* _node){
    this->rotate_left(_node->lchild);
    this->rotate_right(_node);
}

int AVL::diff(const Node* _node) const{
    return this->height(_node->lchild) - this->height(_node->rchild);
}

void AVL::balance(Node* _node){
    if(_node == nullptr){
        return;
    }
    if(this->diff(_node) == -2){
        if(this->diff(_node->rchild) == -1 || this->diff(_node->rchild) == 0){
            this->rotate_left(_node);
        }
        else{
            this->big_rotate_left(_node);
        }
    }
    if(this->diff(_node) == 2){
        if(this->diff(_node->lchild) == 1 || this->diff(_node->lchild) == 0){
            this->rotate_right(_node);
        }
        else{
            this->big_rotate_right(_node);
        }
    }
    this->fix_height(_node);
    this->fix_size(_node);
}

void AVL::insert(int _key){
    Node* newNode = new Node(_key);
    if(root == nullptr){
        root = newNode;
        return;
    }
    Node* pointer = root;
    stack<Node*> toBalance;
    while(true){
        toBalance.push(pointer);
        if(_key < pointer->key){
            if(pointer->lchild == nullptr){
                pointer->lchild = newNode;
                break;
            }
            else{
                pointer = pointer->lchild;
            }
        }
        else{
            if(pointer->rchild == nullptr){
                pointer->rchild = newNode;
                break;
            }
            else{
                pointer = pointer->rchild;
            }
        }
    }
    while(!toBalance.empty()){
        this->balance(toBalance.top());
        toBalance.pop();
    }
    pointer = nullptr;
}

void AVL::state(){
    queue<Node*> q;
    q.push(root);
    Node* it;
    root->lvl = 1;
    while(q.size()){
        it = q.front();
        if(it->lchild != nullptr){
            q.push(it->lchild);
            it->lchild->lvl = it->lvl + 1;
        }
        if(it->rchild != nullptr){
            q.push(it->rchild);
            it->rchild->lvl = it->lvl + 1;
        }
        for(int i = 0; i < it->lvl; ++i){
            cout << '\t';
        }
        cout << it->key << '.' << it->height << '\n';
        q.pop();
    }
    it = nullptr;
    cout << '\n' << '\n';
}

void AVL::remove(int _key){
    Node* pointer = root;
    stack<Node*> toBalance;
    if(root->key == _key){
        if(root->lchild == nullptr){
            root = root->lchild;
            pointer->rchild = nullptr;
            delete pointer;
            return;
        }
        else{
            if(root->rchild == nullptr){
                root = root->lchild;
                pointer->lchild = nullptr;
                delete pointer;
                return;
            }
            else{
                toBalance.push(pointer);
                Node* pointer2 = pointer->rchild;
                while(pointer2->lchild != nullptr){
                    toBalance.push(pointer2);
                    pointer2 = pointer2->lchild;
                }
                swap(pointer->key, pointer2->key);
                if(pointer2 == pointer->rchild){
                    pointer->rchild = pointer2->rchild;
                    pointer2->rchild = nullptr;
                    delete pointer2;
                }
                else{
                    toBalance.top()->lchild = pointer2->rchild;
                    pointer2->rchild = nullptr;
                    delete pointer2;
                }
                pointer2 = nullptr;
                pointer = nullptr;
            }
        }
    }
    else{
        while(pointer->key != _key){
            toBalance.push(pointer);
            if(pointer->key > _key){
                pointer = pointer->lchild;
            }
            else{
                pointer = pointer->rchild;
            }
        }
        if(pointer->lchild == nullptr){
            if(toBalance.top()->lchild == pointer){
                toBalance.top()->lchild = pointer->rchild;
                pointer->rchild = nullptr;
                delete pointer;
            }
            else{
                toBalance.top()->rchild = pointer->rchild;
                pointer->rchild = nullptr;
                delete pointer;
            }
        }
        else{
            if(pointer->rchild == nullptr){
                if(toBalance.top()->lchild == pointer){
                    toBalance.top()->lchild = pointer->lchild;
                    pointer->lchild = nullptr;
                    delete pointer;
                }
                else{
                    toBalance.top()->rchild = pointer->lchild;
                    pointer->lchild = nullptr;
                    delete pointer;
                }
            }
            else{
                toBalance.push(pointer);
                Node* pointer2 = pointer->rchild;
                while(pointer2->lchild != nullptr){
                    toBalance.push(pointer2);
                    pointer2 = pointer2->lchild;
                }
                swap(pointer->key, pointer2->key);
                if(pointer2 == pointer->rchild){
                    pointer->rchild = pointer2->rchild;
                    pointer2->rchild = nullptr;
                    delete pointer2;
                }
                else{
                    toBalance.top()->lchild = pointer2->rchild;
                    pointer2->rchild = nullptr;
                    delete pointer2;
                }
                pointer2 = nullptr;
            }
        }
    }
    while(!toBalance.empty()){
        this->balance(toBalance.top());
        toBalance.pop();
    }
    pointer = nullptr;
}

int AVL::size() const{
    return this->size(root);
}

int AVL::get_place(int _key) const{
    Node* pointer = root;
    int place = 0;
    while(pointer->key != _key){
        if(pointer->key > _key){
            pointer = pointer->lchild;
        }
        else{
            place += this->size(pointer) - this->size(pointer->rchild);
            pointer = pointer->rchild;
        }
    }
    place += this->size(pointer->lchild);
    pointer = nullptr;
    return this->size(root) - 1 - place;
}

int AVL::get_key(int _place) const{
    Node* pointer = root;
    while(true){
        if(this->size(pointer->lchild) == _place){
            int _key = pointer->key;
            pointer = nullptr;
            return _key;
        }
        if(this->size(pointer->lchild) > _place){
            pointer = pointer->lchild;
        }
        else{
            _place -= this->size(pointer) - this->size(pointer->rchild);
            pointer = pointer->rchild;
        }
    }
}

int main(){
    int N;
    cin >> N;
    AVL tree;
    int command;
    int value;
    for(int i = 0; i < N; ++i){
        cin >> command >> value;
        if(command == 1){
            tree.insert(value);
            cout << tree.get_place(value) << '\n';
        }
        else{
            tree.remove(tree.get_key(tree.size() - 1 - value));
        }
    }
    return 0;
}
