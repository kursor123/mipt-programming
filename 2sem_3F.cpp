/*Шрек и Фиона пригласили всех своих друзей на свою свадьбу.
На церемонии они хотят рассадить их всех на две непустые части так,
чтобы количество знакомств между двумя частями было минимальным. Всего приглашенных на свадьбу n, а каждое знакомство обоюдно.
Вам дан граф, в котором ребро означает знакомство между людьми. Помогите Шреку и Фионе поделить гостей на две непустые части.*/
#include <iostream>
#include <vector>
#include <string>
#include <limits>

using namespace std;

class Graph{
public:
    explicit Graph(const vector<string>& _edges);
    explicit Graph(size_t n): edges(n, vector<short>(n)){}
    void AddEdge(size_t first, size_t second);
    void RemoveEdge(size_t first, size_t second);
    size_t Size() const;
    vector<short> GetNeighbours(size_t vertex) const;
private:
    vector<vector<short>> edges;
};

Graph::Graph(const vector<string>& _edges){
    for(size_t i = 0; i < _edges.size(); ++i){
        edges.push_back(vector<short>(_edges[i].size()));
        for(size_t j = 0; j < _edges[i].size(); ++j){
            char c = _edges[i][j];
            edges[i][j] = stoi(&c);
        }
    }
}

void Graph::AddEdge(size_t first, size_t second){
    edges[first][second]++;
}

void Graph::RemoveEdge(size_t first, size_t second){
    edges[first][second] -= ((edges[first][second] > 0)?1:0);
}

size_t Graph::Size() const{
    return edges.size();
}

vector<short> Graph::GetNeighbours(size_t vertex) const{
    return edges[vertex];
}

struct ResidualNetwork{
    ResidualNetwork(size_t _maxFlow, const Graph& _network): maxFlow(_maxFlow), network(_network){}
    size_t maxFlow;
    Graph network;
};

bool operator<(const ResidualNetwork& a, const ResidualNetwork& b){
    return a.maxFlow < b.maxFlow;
}

bool FindPathBetween(const Graph& g, vector<bool>& used, vector<size_t>& path, size_t s, size_t t){
    used[s] = true;
    path.push_back(s);
    if(s == t){
        return true;
    }
    vector<short> neighbours = g.GetNeighbours(s);
    for(size_t i = 0; i < neighbours.size(); ++i){
        if(!neighbours[i]){
            continue;
        }
        if(used[i]){
            continue;
        }
        if(FindPathBetween(g, used, path, i, t)){
            return true;
        }
    }
    path.pop_back();
    return false;
}

ResidualNetwork FindMaxFlow(const Graph& g, size_t source, size_t sink){
    size_t maxFlow = 0;
    Graph resNet(g);
    vector<size_t> path;
    vector<bool> used(g.Size());
    while(FindPathBetween(resNet, used, path, source, sink)){
        maxFlow++;
        for(size_t i = 1; i < path.size(); ++i){
            resNet.RemoveEdge(path[i - 1], path[i]);
            resNet.AddEdge(path[i], path[i - 1]);
        }
        path.clear();
        used.assign(g.Size(), false);
    }
    return ResidualNetwork(maxFlow, resNet);
}

void FindAccessibleVertices(const Graph& g, vector<bool>& used, size_t vertex){
    used[vertex] = true;
    vector<short> neighbours = g.GetNeighbours(vertex);
    for(size_t i = 0; i < neighbours.size(); ++i){
        if(!neighbours[i]){
            continue;
        }
        if(used[i]){
            continue;
        }
        FindAccessibleVertices(g, used, i);
    }
}

pair<vector<size_t>, vector<size_t>> FindMinSlice(const Graph& g){
    //Поиск минимального разреза реализован с помощью метода Форда-Фалкерсона
    ResidualNetwork minSlice(numeric_limits<size_t>::max(), g);
    for(size_t i = 1; i < g.Size(); ++i){
        minSlice = min(minSlice, FindMaxFlow(g, 0, i));
    }
    vector<bool> used(g.Size(), false);
    FindAccessibleVertices(minSlice.network, used, 0);
    pair<vector<size_t>, vector<size_t>> pathes;
    for(size_t i = 0; i < used.size(); ++i){
        if(used[i]){
            pathes.first.push_back(i);
        }
        else{
            pathes.second.push_back(i);
        }
    }
    return pathes;
}

int main(){
    size_t n;
    cin >> n;
    vector<string> edges;
    for(size_t i = 0; i < n; ++i){
        string s;
        cin >> s;
        edges.push_back(s);
    }
    Graph g(edges);
    auto pathes = FindMinSlice(g);
    for(size_t i : pathes.first){
        cout << i + 1 << ' ';
    }
    cout << endl;
    for(size_t i : pathes.second){
        cout << i + 1 << ' ';
    }
    return 0;
}
