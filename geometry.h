#include <iostream>
#include <vector>
#include <cmath>
#include <limits>

using namespace std;

bool eq(double x, double y){
    return (abs(x - y) < 1e-9);
}

struct Point{
    Point(double _x, double _y): x(_x), y(_y){}
    Point() = default;
    bool operator==(const Point& p2) const{return (eq(x, p2.x) && eq(y, p2.y));}
    bool operator!=(const Point& p2) const{return !(*this == p2);}
    Point& operator+=(const Point& p2);
    Point& operator*=(double k);
    Point& operator-=(const Point& p2);
    Point& operator/=(double k);
    void reflex(Point center);
    double len();
    double x = 0;
    double y = 0;
};

const double pi = acos(-1);

Point& Point::operator+=(const Point& p2){
    x += p2.x;
    y += p2.y;
    return *this;
}

Point operator+(const Point& p1, const Point& p2){
    Point newP = p1;
    newP += p2;
    return newP;
}

Point& Point::operator-=(const Point& p2){
    x -= p2.x;
    y -= p2.y;
    return *this;
}

Point operator-(const Point& p1, const Point& p2){
    Point newP = p1;
    newP -= p2;
    return newP;
}

Point& Point::operator*=(double k){
    x *= k;
    y *= k;
    return *this;
}

Point operator*(const Point& p1, double k){
    Point newP = p1;
    newP *= k;
    return newP;
}

Point operator*(double k, const Point& p1){
    return p1 * k;
}

Point& Point::operator/=(double k){
    x /= k;
    y /= k;
    return *this;
}

Point operator/(const Point& p1, double k){
    Point newP = p1;
    newP /= k;
    return newP;
}

void Point::reflex(Point center){
    Point newP = center;
    newP *= 2;
    newP -= *this;
    *this = newP;
}

Point RotatePoint(const Point& p, double angle, bool rad = false){
    if(!rad){
        (angle *= pi) /= 180;
    }
    double newX = p.x * cos(angle) - p.y * sin(angle);
    newX = (eq(newX, 0)?0:newX);
    double newY = p.x * sin(angle) + p.y * cos(angle);
    newY = (eq(newY, 0)?0:newY);
    return Point(newX, newY);
}

double Point::len(){
    return sqrt(x * x + y * y);
}

double pDist(Point p1, Point p2){
    return sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
}

double GetAngle(Point p1, Point p2){
    return (p1.x * p2.x + p1.y * p2.y) / (p1.len() * p2.len());
}

double det(const Point& a, const Point& b, const Point& c){
    double x, y, z;
    x = b.x * c.y - b.y * c.x;
    y = a.x * c.y - a.y * c.x;
    z = a.x * b.y - a.y * b.x;
    return x - y + z;
}

double det(const Point& a, const Point& b){
    return a.x * b.y - b.x * a.y;
}

class Line{
public:
    Line(double k, double b): ax(-k), by(1), c(-b){}
    Line(const Point& p1, const Point& p2);
    Line(const Point& p, double k);
    Line(double _ax, double _by, double _c): ax(_ax), by(_by), c(_c){}
    friend bool operator==(const Line& l1, const Line& l2);
    friend bool operator!=(const Line& l1, const Line& l2);
    friend ostream& operator<<(ostream& out, const Line& l);
    double getA() const;
    double getB() const;
    double getC() const;
private:
    double ax;
    double by;
    double c;
};

Line::Line(const Point& p1, const Point& p2){
    Point normal = RotatePoint(Point(p1.x - p2.x, p1.y - p2.y), 90);
    ax = normal.x;
    by = normal.y;
    c = -(normal.x * p1.x + normal.y * p1.y);
}

Line::Line(const Point& p, double k){
    Point newP(1, k);
    newP += p;
    *this = Line(p, newP);
}

double Line::getA() const{
    return ax;
}

double Line::getB() const{
    return by;
}

double Line::getC() const{
    return c;
}

bool operator==(const Line& l1, const Line& l2){
    if(eq(l1.ax, 0) && eq(l1.by, 0) && eq(l1.c, 0)){
        return (eq(l2.ax, 0) && eq(l2.by, 0) && eq(l2.c, 0))?true:false;
    }
    vector<double> iterL1 = {l1.ax, l1.by, l1.c};
    vector<double> iterL2 = {l2.ax, l2.by, l2.c};
    int num = -1;
    for(size_t i = 0; i < iterL1.size(); ++i){
        if(!eq(iterL1[i], 0)){
            num = i;
            break;
        }
    }
    if(eq(iterL2[num], 0)){
        return false;
    }
    double cf1 = iterL1[num];
    double cf2 = iterL2[num];
    for(size_t i = 0; i < iterL1.size(); ++i){
        iterL1[i] /= cf1;
        iterL2[i] /= cf2;
    }
    return eq(iterL1[0], iterL2[0]) && eq(iterL1[1], iterL2[1]) && eq(iterL1[2], iterL2[2]);
}

bool operator!=(const Line& l1, const Line& l2){
    return !(l1 == l2);
}

Point ReflexPointLine(Point p, Line axis){
    double a = axis.getA();
    double b = axis.getB();
    double c = axis.getC();
    double x = p.x;
    double y = p.y;
    double dist = abs(a * x + b * y + c) / sqrt(a * a + b * b);
    Point normal(a, b);
    normal.x /= sqrt(a * a + b * b);
    normal.x *= dist;
    normal.y /= sqrt(a * a + b * b);
    normal.y *= dist;
    Point newP = p;
    newP += normal;
    return (eq(newP.x * a + newP.y * b + c, 0)?(newP + normal):(newP - normal * 3));
}

double LineValue(const Point& p, const Line& l){
    return p.x * l.getA() + p.y * l.getB() + l.getC();
}

bool AreByOneSide(Point p1, Point p2, Line l){
    bool c1 = LineValue(p1, l) + 1e-9 < 0;
    bool c2 = LineValue(p2, l) + 1e-9 < 0;
    return !(c1 ^ c2);
}

Point Intersection(const Line& l1, const Line& l2){
    double a1 = l1.getA();
    double a2 = l2.getA();
    double b1 = l1.getB();
    double b2 = l2.getB();
    double c1 = -l1.getC();
    double c2 = -l2.getC();
    Point a(a1, a2);
    Point b(b1, b2);
    Point c(c1, c2);
    Point newP(det(c, b) / det(a, b), det(a, c) / det(a, b));
    newP.x = (eq(newP.x, 0)?0:newP.x);
    newP.y = (eq(newP.y, 0)?0:newP.y);
    return newP;
}

class Shape{
public:
    virtual ~Shape() = default;
    virtual double perimeter() const = 0;
    virtual double area() const = 0;
    virtual bool operator==(const Shape& another) const = 0;
    virtual bool operator!=(const Shape& another) const = 0;
    virtual bool isCongruentTo(const Shape& another) const = 0;
    virtual bool isSimilarTo(const Shape& another) const = 0;
    virtual bool containsPoint(Point point) const = 0;
    virtual void rotate(Point center, double angle) = 0;
    virtual void reflex(Point center) = 0;
    virtual void reflex(Line axis) = 0;
    virtual void scale(Point center, double coefficient) = 0;
};

class Ellipse: public Shape{
public:
    Ellipse(Point _f1, Point _f2, double _fDist): f1(_f1), f2(_f2), fDist(_fDist){}
    Ellipse() = default;
    ~Ellipse() = default;
    std::pair<Point, Point> focuses() const;
    std::pair<Line, Line> directrices() const;
    double eccentricity() const;
    Point center() const;
    double perimeter() const;
    double area() const;
    bool operator==(const Shape& another) const;
    bool operator!=(const Shape& another) const;
    bool isCongruentTo(const Shape& another) const;
    bool isSimilarTo(const Shape& another) const;
    bool containsPoint(Point point) const;
    void rotate(Point center, double angle);
    void reflex(Point center);
    void reflex(Line axis);
    void scale(Point center, double coefficient);
    double getDist() const;
protected:
    Point f1;
    Point f2;
    double fDist;
};

double Ellipse::getDist() const{
    return fDist;
}

std::pair<Point, Point> Ellipse::focuses() const{
    return make_pair(f1, f2);
}

std::pair<Line, Line> Ellipse::directrices() const{
    if(eccentricity() == 0){
        return make_pair(Line(0, 0, 1), Line(0, 0, 1));
    }
    double a = fDist / 2;
    Point dir = f2;
    dir -= f1;
    Point normal = RotatePoint(dir, 90);
    Point p1 = center() + (dir / dir.len()) * a / eccentricity();
    Point p2 = center() - (dir / dir.len()) * a / eccentricity();
    return make_pair(Line(p1, p1 + normal), Line(p2, p2 + normal));
}

double Ellipse::eccentricity() const{
    double a = fDist / 2;
    double c = pDist(f1, f2) / 2;
    return c / a;
}

Point Ellipse::center() const{
    Point newP = (f1 + f2) / 2;
    return newP;
}

double Ellipse::perimeter() const{
    double a = fDist / 2;
    double c = pDist(f1, f2) / 2;
    double b = sqrt(a * a - c * c);
    double usefulConst = (3 * (a - b) * (a - b) / ((a + b) * (a + b)));
    return (pi * (a + b) * (1 + usefulConst / (10 + sqrt(4 - usefulConst))));
}

double Ellipse::area() const{
    double a = fDist / 2;
    double c = pDist(f1, f2) / 2;
    double b = sqrt(a * a - c * c);
    return pi * b * a;
}

bool Ellipse::operator==(const Shape& another) const{
    auto ptr = dynamic_cast<const Ellipse*>(&another);
    if(!ptr){
        return false;
    }
    const Ellipse& el = static_cast<const Ellipse&>(another);
    pair<Point, Point> focs = el.focuses();
    return (((focs.first == f1 && focs.second == f2) || (focs.first == f2 && focs.second == f1)) && eq(fDist, el.getDist()));
}

bool Ellipse::operator!=(const Shape& another) const{
    return !((*this) == another);
}

bool Ellipse::isCongruentTo(const Shape& another) const{
    auto ptr = dynamic_cast<const Ellipse*>(&another);
    if(!ptr){
        return false;
    }
    const Ellipse& el = static_cast<const Ellipse&>(another);
    pair<Point, Point> focs = el.focuses();
    Point fcsDif = focs.first - focs.second;
    return eq((f2 - f1).len(), fcsDif.len()) && eq(fDist, el.getDist());
}

bool Ellipse::isSimilarTo(const Shape& another) const{
    auto ptr = dynamic_cast<const Ellipse*>(&another);
    if(!ptr){
        return false;
    }
    const Ellipse& el = static_cast<const Ellipse&>(another);
    pair<Point, Point> focs = el.focuses();
    return eq((f1 - f2).len() / fDist, (focs.first - focs.second).len() / el.getDist());
}

bool Ellipse::containsPoint(Point point) const{
    return fDist - pDist(f1, point) - pDist(f2, point) >= 1e-9;
}

void Ellipse::rotate(Point center, double angle){
    f1 -= center;
    f2 -= center;
    f1 = RotatePoint(f1, angle);
    f2 = RotatePoint(f2, angle);
    f1 += center;
    f2 += center;
}

void Ellipse::reflex(Point center){
    f1.reflex(center);
    f2.reflex(center);
}

void Ellipse::reflex(Line axis){
    f1 = ReflexPointLine(f1, axis);
    f2 = ReflexPointLine(f2, axis);
}

void Ellipse::scale(Point center, double coefficient){
    f1 -= center;
    f2 -= center;
    f1.x *= coefficient;
    f2.x *= coefficient;
    f1.y *= coefficient;
    f2.y *= coefficient;
    f1 += center;
    f2 -= center;
    fDist *= coefficient;
}

class Circle: public Ellipse{
public:
    Circle(const Point& center, double radius): Ellipse(center, center, 2 * radius){}
    Circle(const Point& p1, const Point& p2, const Point& p3);
    double radius() const;
};

Circle::Circle(const Point& p1, const Point& p2, const Point& p3){
    Line edge1(p1, p2);
    Line edge2(p1, p3);
    f1 = f2 = Intersection(Line((p1 + p2) / 2, (p1 + p2) / 2 + Point(edge1.getA(), edge1.getB())),
                            Line((p1 + p3) / 2, (p1 + p3) / 2 + Point(edge2.getA(), edge2.getB())));
    fDist = 2 * ((f1 - p1).len());
}

double Circle::radius() const{
    return getDist() / 2;
}

class Polygon: public Shape{
public:
    Polygon(const vector<Point>& _vertices): vertices(_vertices){}
    template<typename T, typename... Args>
    Polygon(T p, Args... arg){push(p, arg...);}
    ~Polygon() = default;
    size_t verticesCount() const;
    std::vector<Point> getVertices() const;
    bool isConvex() const;
    double perimeter() const;
    double area() const;
    bool operator==(const Shape& another) const;
    bool operator!=(const Shape& another) const;
    bool isCongruentTo(const Shape& another) const;
    bool isSimilarTo(const Shape& another) const;
    bool containsPoint(Point point) const;
    void rotate(Point center, double angle);
    void reflex(Point center);
    void reflex(Line axis);
    void scale(Point center, double coefficient);
protected:
    void push(){};
    template<typename T, typename... Args>
    void push(T value, Args... arg){
        vertices.push_back(value);
        push(arg...);
    }
    vector<Point> vertices;
};

size_t Polygon::verticesCount() const{
    return vertices.size();
}

std::vector<Point> Polygon::getVertices() const{
    vector<Point> newV = vertices;
    return newV;
}

bool Polygon::isConvex() const{
    bool neg = det(vertices[0], vertices[1], vertices[2]) + 1e-9 < 0;
    for(size_t i = 1; i < vertices.size(); ++i){
        if((det(vertices[i], vertices[(i + 1) % vertices.size()], vertices[(i + 2) % vertices.size()])
           + 1e-9 < 0) ^ neg){
            return false;
        }
    }
    return true;
}

double Polygon::perimeter() const{
    double p = 0;
    for(size_t i = 0; i < vertices.size(); ++i){
        p += (vertices[i] - vertices[(i + 1) % vertices.size()]).len();
    }
    return p;
}

double Polygon::area() const{
    double area = 0;
    for(size_t i = 0; i < vertices.size(); ++i){
        area += ((vertices[(i + 1) % vertices.size()].x - vertices[i].x) *
        (vertices[(i + 1) % vertices.size()].y + vertices[i].y) / 2);
    }
    return abs(area);
}

bool Polygon::operator==(const Shape& another) const{
    auto ptr = dynamic_cast<const Polygon*>(&another);
    if(!ptr){
        return false;
    }
    vector<Point> pol = static_cast<const Polygon&>(another).getVertices();
    if(vertices.size() != pol.size()){
        return false;
    }
    size_t ind = 0;
    bool found = false;
    for(size_t i = 0; i < pol.size(); ++i){
        if(pol[i] == vertices[0]){
            found = true;
            ind = i;
        }
    }
    if(!found){
        return false;
    }
    bool same = (pol[(ind + 1) % pol.size()] == vertices[1]);
    for(size_t i = 0; i < pol.size(); ++i){
        if(vertices[i] != pol[(static_cast<int>(ind + pol.size()) +
                        (same?static_cast<int>(i):(-static_cast<int>(i)))) % pol.size()]){
            return false;
        }
    }
    return true;
}

bool Polygon::operator!=(const Shape& another) const{
    return !((*this) == another);
}

bool Polygon::isCongruentTo(const Shape& another) const{
    auto ptr = dynamic_cast<const Polygon*>(&another);
    if(!ptr){
        return false;
    }
    vector<Point> pol = static_cast<const Polygon&>(another).getVertices();
    if(vertices.size() != pol.size()){
        return false;
    }
    double ang = GetAngle(vertices[0] - vertices[1], vertices[2] - vertices[1]);
    for(size_t i = 0; i < pol.size(); ++i){
        Point p1 = pol[(i + pol.size() - 1) % pol.size()];
        Point p2 = pol[i];
        Point p3 = pol[(i + 1) % pol.size()];
        double cAng = GetAngle(p1 - p2, p3 - p2);
        if(eq(cAng, ang)){
            for(int s = -1; s < 2; ++s){
                if(s == 0){
                    continue;
                }
                bool isFound = true;
                for(int j = 0; j < pol.size(); ++j){
                    Point p1 = vertices[j];
                    Point p2 = vertices[(j + 1) % pol.size()];
                    Point p3 = vertices[(j + 2) % pol.size()];
                    Point q1 = pol[(i + s * j + pol.size() - s) % pol.size()];
                    Point q2 = pol[(i + s * j + pol.size()) % pol.size()];
                    Point q3 = pol[(i + s * j + s + pol.size()) % pol.size()];
                    if(!eq(GetAngle(p1 - p2, p3 - p2), GetAngle(q1 - q2, q3 - q2))){
                        isFound = false;
                        break;
                    }
                    if(!eq((p1 - p2).len(), (q1 - q2).len()) || !eq((p3 - p2).len(), (q3 - q2).len())){
                        isFound = false;
                        break;
                    }
                }
                if(isFound){
                    return true;
                }
            }
        }
    }
    return false;
}

bool Polygon::isSimilarTo(const Shape& another) const{
    auto ptr = dynamic_cast<const Polygon*>(&another);
    if(!ptr){
        return false;
    }
    vector<Point> pol = static_cast<const Polygon&>(another).getVertices();
    if(vertices.size() != pol.size()){
        return false;
    }
    double ang = GetAngle(vertices[0] - vertices[1], vertices[2] - vertices[1]);
    for(size_t i = 0; i < pol.size(); ++i){
        Point p1 = pol[(i + pol.size() - 1) % pol.size()];
        Point p2 = pol[i];
        Point p3 = pol[(i + 1) % pol.size()];
        double cAng = GetAngle(p1 - p2, p3 - p2);
        if(eq(cAng, ang)){
            for(int s = -1; s < 2; ++s){
                if(s == 0){
                    continue;
                }
                bool isFound = true;
                for(int j = 0; j < pol.size(); ++j){
                    Point p1 = vertices[j];
                    Point p2 = vertices[(j + 1) % pol.size()];
                    Point p3 = vertices[(j + 2) % pol.size()];
                    Point q1 = pol[(i + s * j + pol.size() - s) % pol.size()];
                    Point q2 = pol[(i + s * j + pol.size()) % pol.size()];
                    Point q3 = pol[(i + s * j + s + pol.size()) % pol.size()];
                    if(!eq(GetAngle(p1 - p2, p3 - p2), GetAngle(q1 - q2, q3 - q2))){
                        isFound = false;
                        break;
                    }
                    if(!eq((p1 - p2).len() / (p3 - p2).len(), (q1 - q2).len() / (q3 - q2).len())){
                        isFound = false;
                        break;
                    }
                }
                if(isFound){
                    return true;
                }
            }
        }
    }
    return false;
}

bool isBetween(const Point& b, const Point& p1, const Point& p2){
    if(b == p1){
        return false;
    }
    bool byX = (b.x <= p1.x + 1e-9 && b.x + 1e-9 >= p2.x) || (b.x + 1e-9 >= p1.x && b.x <= p2.x + 1e-9);
    bool byY = (b.y <= p1.y + 1e-9 && b.y + 1e-9 >= p2.y) || (b.y + 1e-9 >= p1.y && b.y <= p2.y + 1e-9);
    return byX && byY;
}

bool Polygon::containsPoint(Point point) const{
    vector<Point> pts = vertices;
    for(size_t i = 0; i < pts.size(); ++i){
        pts[i] -= point;
    }
    int intCount = 0;
    Line l = Line(Point(0, 0), Point(0, 1));
    for(size_t i = 0; i < pts.size(); ++i){
        Point left = pts[i];
        Point right = pts[(i + 1) % pts.size()];
        Line edge(left, right);
        if(eq(LineValue(Point(0, 0), edge), 0)){
            return true;
        }
        if(eq(right.x, left.x)){
            continue;
        }
        Point intr = Intersection(edge, l);
        if(intr.y + 1e-9 >= 0 && isBetween(intr, left, right)){
            intCount++;
            if(right.x == pts[(i + 2) % pts.size()].x){
                ++i;
            }
        }
    }
    return (intCount % 2 == 1);
}

void Polygon::rotate(Point center, double angle){
    for(size_t i = 0; i < vertices.size(); ++i){
        vertices[i] -= center;
        vertices[i] = RotatePoint(vertices[i], angle);
        vertices[i] += center;
    }
}

void Polygon::reflex(Point center){
    for(size_t i = 0; i < vertices.size(); ++i){
        vertices[i].reflex(center);
    }
}

void Polygon::reflex(Line axis){
    for(size_t i = 0; i < vertices.size(); ++i){
        vertices[i] = ReflexPointLine(vertices[i], axis);
    }
}

void Polygon::scale(Point center, double coefficient){
    for(size_t i = 0; i < vertices.size(); ++i){
        vertices[i] -= center;
        vertices[i] *= coefficient;
        vertices[i] += center;
    }
}

class Rectangle: public Polygon{
public:
    Rectangle(const Point& p1, const Point& p2, double k);
    Point center() const;
    std::pair<Line, Line> diagonals() const;
};

Rectangle::Rectangle(const Point& p1, const Point& p2, double k): Polygon(p1,
    p1 + RotatePoint((p2 - p1) * cos(atan(((k >= 1)?k:(1 / k)))), atan(((k >= 1)?k:(1 / k))), true),
    p2, p2 - RotatePoint((p2 - p1) * cos(atan(((k >= 1)?k:(1 / k)))), atan(((k >= 1)?k:(1 / k))), true)){}

Point Rectangle::center() const{
    return (vertices[0] + vertices[2]) / 2;
}

std::pair<Line, Line> Rectangle::diagonals() const{
    return make_pair(Line(vertices[0], vertices[2]), Line(vertices[1], vertices[3]));
}

class Square: public Rectangle{
public:
    Square(Point p1, Point p2): Rectangle(p1, p2, 1){}
    Circle circumscribedCircle() const;
    Circle inscribedCircle() const;
};

Circle Square::circumscribedCircle() const{
    return Circle(center(), (vertices[0] - vertices[1]).len() / sqrt(2));
}

Circle Square::inscribedCircle() const{
    return Circle(center(), (vertices[0] - vertices[1]).len() / 2);
}

class Triangle: public Polygon{
public:
    Triangle(Point p1, Point p2, Point p3): Polygon(p1, p2, p3){}
    Circle circumscribedCircle() const;
    Circle inscribedCircle() const;
    Point centroid() const;
    Point orthocenter() const;
    Line EulerLine() const;
    Circle ninePointsCircle() const;
};

Circle Triangle::circumscribedCircle() const{
    return Circle(vertices[0], vertices[1], vertices[2]);
}

Circle Triangle::inscribedCircle() const{
    double p = perimeter() / 2;
    Point p1 = vertices[0] + (vertices[1] - vertices[0]) *
            (p - (vertices[1] - vertices[2]).len()) / (vertices[1] - vertices[0]).len();
    Point p2 = vertices[0] + (vertices[2] - vertices[0]) *
            (p - (vertices[1] - vertices[2]).len()) / (vertices[2] - vertices[0]).len();
    Point p3 = vertices[1] + (vertices[2] - vertices[1]) *
            (p - (vertices[2] - vertices[0]).len()) / (vertices[2] - vertices[1]).len();
    return Circle(p1, p2, p3);
}

Point Triangle::centroid() const{
    return (vertices[0] + vertices[1] + vertices[2]) / 3;
}

Point Triangle::orthocenter() const{
    Circle csc = circumscribedCircle();
    Point c = csc.center();
    return vertices[0] + vertices[1] + vertices[2] - c * 2;
}

Line Triangle::EulerLine() const{
    return Line(circumscribedCircle().center(), centroid());
}

Circle Triangle::ninePointsCircle() const{
    Circle csc = circumscribedCircle();
    return Circle((csc.center() + orthocenter()) / 2, csc.radius() / 2);
}
