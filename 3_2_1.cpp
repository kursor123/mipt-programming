#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <assert.h>
#include <queue>

using namespace std;

struct Node{
    int key;
    Node* lchild = nullptr;
    Node* rchild = nullptr;
    int height = 1;
    Node(int _key) : key(_key){}
    ~Node(){
        delete lchild;
        delete rchild;
    }
};

struct DNode{
    int key;
    int prior;
    DNode* lchild = nullptr;
    DNode* rchild = nullptr;
    int height = 1;
    DNode(int _key, int _prior) : key(_key), prior(_prior){}
    ~DNode(){
        delete lchild;
        delete rchild;
    }
};

class BST{
    public:
        void insert(int _key);
        BST() = default;
        ~BST(){
            delete root;
        }
        int height(){
            return treeHeight;
        }
    private:
        Node* root = nullptr;
        int treeHeight = 0;
};

void BST::insert(int _key){
    Node* newNode = new Node(_key);
    if(root == nullptr){
        root = newNode;
        treeHeight = 1;
        return;
    }
    Node* pointer = root;
    while(true){
        if(_key < pointer->key){
            if(pointer->lchild == nullptr){
                pointer->lchild = newNode;
                pointer->lchild->height = pointer->height + 1;
                if(pointer->lchild->height > treeHeight){
                    treeHeight = pointer->lchild->height;
                }
                break;
            }
            else{
                pointer = pointer->lchild;
            }
        }
        else{
            if(pointer->rchild == nullptr){
                pointer->rchild = newNode;
                pointer->rchild->height = pointer->height + 1;
                if(pointer->rchild->height > treeHeight){
                    treeHeight = pointer->rchild->height;
                }
                break;
            }
            else{
                pointer = pointer->rchild;
            }
        }
    }
}

class DBST{
    public:
        void insert(int _key, int _prior);
        DBST(DNode* _root){
            root = _root;
        }
        DBST() = default;
        ~DBST(){
            delete root;
        }
        int height();
        void state();
        friend pair<DNode*, DNode*> Split(DNode* _root, int _key);
    private:
        DNode* root = nullptr;
};

pair<DNode*, DNode*> Split(DNode* _root, int _key){
    if(_root == nullptr){
        pair<DNode*, DNode*> trees(nullptr, nullptr);
        return trees;
    }
    else{
        if(_root->key < _key){
            if(_root->rchild == nullptr){
                pair<DNode*, DNode*> trees(_root, nullptr);
                return trees;
            }
            pair<DNode*, DNode*> trees = Split(_root->rchild, _key);
            _root->rchild = trees.first;
            trees.first = _root;
            return trees;
        }
        else{
            if(_root->lchild == nullptr){
                pair<DNode*, DNode*> trees(nullptr, _root);
                return trees;
            }
            pair<DNode*, DNode*> trees = Split(_root->lchild, _key);
            _root->lchild = trees.second;
            trees.second = _root;
            return trees;
        }
    }
}

void DBST::state(){
    queue<DNode*> q;
    q.push(root);
    DNode* it;
    root->height = 1;
    while(q.size()){
        it = q.front();
        if(it->lchild != nullptr){
            q.push(it->lchild);
            it->lchild->height = it->height + 1;
        }
        if(it->rchild != nullptr){
            q.push(it->rchild);
            it->rchild->height = it->height + 1;
        }
        for(int i = 0; i < it->height; ++i){
            cout << '\t';
        }
        cout << it->key << '.' << it->prior << '\n';
        q.pop();
    }
    it = nullptr;
    cout << '\n' << '\n';
}

void DBST::insert(int _key, int _prior){
    DNode* newNode = new DNode(_key, _prior);
    if(root == nullptr){
        root = newNode;
        return;
    }
    DNode* pointer = root;
    if(root->prior < _prior){
        pair<DNode*, DNode*> trees = Split(root, _key);
        root = newNode;
        root->lchild = trees.first;
        root->rchild = trees.second;
        pointer = nullptr;
        return;
    }
    else{
        pair<DNode*, char> parent (root, 0);
        while(pointer->prior >= _prior){
            if(pointer->key <= _key){
                if(pointer->rchild == nullptr){
                    pointer->rchild = newNode;
                    pointer = nullptr;
                    return;
                }
                else{
                    parent.first = pointer;
                    parent.second = 1;
                    pointer = pointer->rchild;
                }
            }
            else{
                if(pointer->lchild == nullptr){
                    pointer->lchild = newNode;
                    pointer = nullptr;
                    return;
                }
                else{
                    parent.first = pointer;
                    parent.second = 0;
                    pointer = pointer->lchild;
                }
            }
        }
        pair<DNode*, DNode*> trees = Split(pointer, _key);
        if(parent.second == 0){
            parent.first->lchild = newNode;
            parent.first->lchild->lchild = trees.first;
            parent.first->lchild->rchild = trees.second;
        }
        else{
            parent.first->rchild = newNode;
            parent.first->rchild->lchild = trees.first;
            parent.first->rchild->rchild = trees.second;
        }
        pointer = nullptr;
    }
}

int DBST::height(){
    if(root == nullptr){
        return 0;
    }
    queue<DNode*> q;
    q.push(root);
    DNode* it;
    root->height = 1;
    int maxHeight = 1;
    while(q.size()){
        it = q.front();
        if(it->lchild != nullptr){
            q.push(it->lchild);
            it->lchild->height = it->height + 1;
            if(it->lchild->height > maxHeight){
                maxHeight = it->lchild->height;
            }
        }
        if(it->rchild != nullptr){
            q.push(it->rchild);
            it->rchild->height = it->height + 1;
            if(it->rchild->height > maxHeight){
                maxHeight = it->rchild->height;
            }
        }
        q.pop();
    }
    it = nullptr;
    return maxHeight;
}

int main(){
    DBST tree1;
    BST tree2;
    int a, b;
    int n;
    cin >> n;
    for(int i = 0; i < n; ++i){
        cin >> a >> b;
        tree1.insert(a, b);
        tree2.insert(a);
    }
    cout << tree2.height() - tree1.height();
    return 0;
}
