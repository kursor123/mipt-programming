/*Беатрикс Киддо в молодости была наемницей и любила составлять графы связей между её целями.
Она цепляла на стену иголки и связывала их нитками.
Но так как она наполовину японка, ей особенно нравилось, когда цели можно было расположить так, чтобы нитки не пересекались.
Вам известна схема, которую хочет начертить Беатрикс. Выясните, может ли в этот раз схема ей понравиться?*/
#include <iostream>
#include <vector>
#include <limits>

using namespace std;

const int MAXN = 1000;

class Graph{
public:
    explicit Graph(int n);
    Graph() = default;
    ~Graph() = default;
    void AddEdge(int first, int second);
    void RemoveEdge(int first, int second);
    void AddVertex(int vertex);
    void RemoveVertex(int vertex);
    int GetName(int index) const;
    int GetIndex(int name) const;
    int Size() const;
    void SetNeighbours(int name, const vector<int>& neighbours);//устанавливает вершине name вектор соседей neighbours
    const vector<int>& GetNeighbours(int vertex) const;
    vector<vector<bool>> GetMatrix() const;
private:
    vector<int> vertices;
    vector<vector<int>> edges;
};

Graph::Graph(int n): edges(n){
    for(int i = 0; i < n; ++i){
        vertices.push_back(i);
    }
}

class Segment : public Graph{
public:
    Segment() = default;
    void AddContact(int vertex);
    Segment(const Graph& g): Graph(g){}
    int CountFaces(const vector<vector<int>>& faces);
    int GetNumOfFaces() const;
    pair<int, int> GetPairOfContactVertices() const;
private:
    int cfaces = numeric_limits<int>::max();
    vector<int> contVert;
};

void Segment::AddContact(int vertex){
    contVert.push_back(vertex);
}

int Segment::GetNumOfFaces() const{
    return cfaces;
}

pair<int, int> Segment::GetPairOfContactVertices() const{
    return make_pair(contVert[0], contVert[1]);
}

int Segment::CountFaces(const vector<vector<int>>& faces){
    cfaces = 0;
    int ownFace = 0;
    for(int i = 0; i < faces.size(); ++i){
        bool suitable = true;
        for(auto name : contVert){
            bool vertSuit = false;
            for(int j = 0; j < faces[i].size(); ++j){
                if(name == faces[i][j]){
                    vertSuit = true;
                }
            }
            suitable &= vertSuit;
        }
        if(suitable){
            cfaces++;
            ownFace = i;
        }
    }
    return ownFace;
}

int Graph::GetName(int index) const{
    return vertices[index];
}

int Graph::GetIndex(int name) const{
    int index = -1;
    for(size_t i = 0; i < vertices.size(); ++i){
        if(vertices[i] == name){
            index = i;
            break;
        }
    }
    return index;
}

void Graph::AddVertex(int vertex){
    if(GetIndex(vertex) == -1){
        vertices.push_back(vertex);
        edges.push_back(vector<int>());
    }
}

void Graph::AddEdge(int first, int second){
    edges[GetIndex(first)].push_back(second);
    edges[GetIndex(second)].push_back(first);
}

void Graph::RemoveEdge(int first, int second){
    int ind1 = GetIndex(first);
    int ind2 = GetIndex(second);
    int it = 0;
    while(it < edges[ind1].size()){
        if(edges[ind1][it] == second){
            swap(edges[ind1][it], edges[ind1][edges[ind1].size() - 1]);
            edges[ind1].pop_back();
            break;
        }
        it++;
    }
    it = 0;
    while(it < edges[ind2].size()){
        if(edges[ind2][it] == first){
            swap(edges[ind2][it], edges[ind2][edges[ind2].size() - 1]);
            edges[ind2].pop_back();
            break;
        }
        it++;
    }
}

int Graph::Size() const{
    return vertices.size();
}

const vector<int>& Graph::GetNeighbours(int vertex) const{
    return edges[GetIndex(vertex)];
}

void Graph::SetNeighbours(int name, const vector<int>& neighbours){
    edges[GetIndex(name)] = neighbours;
}

vector<vector<bool>> Graph::GetMatrix() const{
    vector<vector<bool>> matrix;
    for(int i = 0; i < Size(); ++i){
        matrix.emplace_back(Size(), false);
    }
    for(int i = 0; i < edges.size(); ++i){
        for(int j = 0; j < edges[i].size(); ++j){
            matrix[i][GetIndex(edges[i][j])] = true;
        }
    }
    return matrix;
}

void Graph::RemoveVertex(int vertex){
    int ind = GetIndex(vertex);
    if(ind == -1){
        return;
    }
    int last = vertices[Size() - 1];
    swap(vertices[ind], vertices.back());
    swap(edges[ind], edges.back());
    for(auto neighbour : edges.back()){
        int neighbourIndex = GetIndex(neighbour);
        for(int i = 0; i < edges[neighbourIndex].size(); ++i){
            if(edges[neighbourIndex][i] == vertex){
                swap(edges[neighbourIndex][i], edges[neighbourIndex].back());
                edges[neighbourIndex].pop_back();
                break;
            }
        }
    }
    edges.pop_back();
    vertices.pop_back();
}

void Diff(Graph& g, const vector<int>& names){
    for(auto name : names){
        if(g.GetIndex(name) != -1){
            g.RemoveVertex(name);
        }
    }
}

void BuildComponent(int walkIt, const Graph& graph, vector<bool>& used, Graph& component){
    used[walkIt] = true;
    component.AddVertex(graph.GetName(walkIt));
    component.SetNeighbours(graph.GetName(walkIt), graph.GetNeighbours(graph.GetName(walkIt)));
    vector<int> neighbours = graph.GetNeighbours(graph.GetName(walkIt));
    for(int i = 0; i < neighbours.size(); ++i){
        if(!used[graph.GetIndex(neighbours[i])]){
            BuildComponent(graph.GetIndex(neighbours[i]), graph, used, component);
        }
    }
}

vector<Graph> DivideByComponents(const Graph& graph){
    vector<Graph> components;
    vector<bool> used(graph.Size(), false);
    int walkIt = 0;
    while(walkIt < graph.Size()){
        while(walkIt < graph.Size() && used[walkIt]){
            walkIt++;
        }
        if(walkIt < graph.Size()){
            Graph component(0);
            BuildComponent(walkIt, graph, used, component);
            components.push_back(component);
            walkIt++;
        }
    }
    return components;
}

int InnerCDFS(int parent, int name, const Graph& graph, vector<int>& walk, vector<bool>& used){
    walk.push_back(name);
    used[name] = true;
    vector<int> neighbours = graph.GetNeighbours(name);
    for(int i = 0; i < neighbours.size(); ++i){
        if(neighbours[i] != parent){
            if(used[neighbours[i]]){
                return neighbours[i];
            }
            int res = InnerCDFS(name, neighbours[i], graph, walk, used);
            if(res >= 0){
                return res;
            }
        }
    }
    walk.pop_back();
    return -1;
}

bool FindCycles(const Graph& graph, vector<int>& cycle){
    cycle.clear();
    vector<bool> used(MAXN, false);
    vector<int> walk;
    int res = InnerCDFS(graph.GetName(0), graph.GetName(0), graph, walk, used);
    if(res >= 0){
        while(walk.back() != res){
            cycle.push_back(walk.back());
            walk.pop_back();
        }
        cycle.push_back(res);
        return true;
    }
    return false;
}

void InnerBDFS(int parent, int name, const Graph& graph, vector<bool>& used,
                vector<int>& inTime, vector<int>& ret, int& time, vector<pair<int, int>>& bridges){
    used[name] = true;
    inTime[name] = time;
    ret[name] = time;
    time++;
    vector<int> neighbours = graph.GetNeighbours(name);
    for(int i = 0; i < neighbours.size(); ++i){
        if(neighbours[i] != parent){
            if(used[neighbours[i]]){
                ret[name] = min(ret[name], inTime[neighbours[i]]);
            }
            else{
                InnerBDFS(name, neighbours[i], graph, used, inTime, ret, time, bridges);
                ret[name] = min(ret[name], ret[neighbours[i]]);
                if(ret[neighbours[i]] > inTime[name]){
                    bridges.emplace_back(name, neighbours[i]);
                }
            }
        }
    }
}

void FindBridgesInCG(const Graph& graph, vector<pair<int, int>>& bridges){
    bridges.clear();
    vector<bool> used(MAXN);
    vector<int> inTime(MAXN);
    vector<int> ret(MAXN);
    int time = 0;
    InnerBDFS(graph.GetName(0), graph.GetName(0), graph, used, inTime, ret, time, bridges);
}

void RemoveBridges(Graph& graph, const vector<pair<int, int>>& bridges){
    for(int i = 0; i < bridges.size(); ++i){
        graph.RemoveEdge(bridges[i].first, bridges[i].second);
    }
}

void BuildEdgesSegments(const Graph& graph, const vector<vector<bool>>& matrix,
                         const vector<vector<int>>& faces, vector<Segment>& segs){
    segs.clear();
    vector<vector<bool>> used;
    for(int i = 0; i < MAXN; ++i){
        used.emplace_back(MAXN, false);
    }
    for(auto cycle : faces){
        used[cycle[0]][cycle[cycle.size() - 1]] = true;
        used[cycle[cycle.size() - 1]][cycle[0]] = true;
        for(int j = 1; j < cycle.size(); ++j){
            used[cycle[j - 1]][cycle[j]] = true;
            used[cycle[j]][cycle[j - 1]] = true;
        }
    }
    for(int i = 0; i < MAXN; ++i){
        used[i][i] = true;
    }
    for(int i = 0; i < faces.size(); ++i){
        for(int j = i; j < faces.size(); ++j){
            for(auto name1 : faces[i]){
                for(auto name2: faces[j]){
                    if(!used[name1][name2] && matrix[graph.GetIndex(name1)][graph.GetIndex(name2)]){
                        Segment s;
                        s.AddContact(name1);
                        s.AddContact(name2);
                        used[name1][name2] = true;
                        used[name2][name1] = true;
                        s.AddVertex(name1);
                        s.AddVertex(name2);
                        s.AddEdge(name1, name2);
                        segs.push_back(s);
                    }
                }
            }
        }
    }
}

void BuildSegments(const Graph& graph, const vector<vector<int>>& faces, vector<Segment>& segs){
    segs.clear();
    vector<bool> used(MAXN, false);
    Graph newGraph = graph;
    for(auto cycle : faces){
        Diff(newGraph, cycle);
    }
    vector<Graph> comps = DivideByComponents(newGraph);
    for(auto g : comps){
        Segment s(g);
        segs.push_back(s);
    }
    for(auto cycle : faces){
        for(auto name : cycle){
            if(used[name]){
                break;
            }
            for(auto nb : graph.GetNeighbours(name)){
                for(int i = 0; i < segs.size(); ++i){
                    if(segs[i].GetIndex(nb) != -1 && !used[nb]){
                        segs[i].AddVertex(name);
                        segs[i].AddEdge(name, nb);
                        segs[i].AddContact(name);
                        used[name] = true;
                    }
                }
            }
            used[name] = true;
        }
    }
}

Segment MinNumFaces(const vector<vector<int>>& faces, vector<Segment>& segs){
    int cfaces = numeric_limits<int>::max();
    size_t index = 0;
    for(size_t i = 0; i < segs.size(); ++i){
        segs[i].CountFaces(faces);
        if(segs[i].GetNumOfFaces() < cfaces){
            index = i;
            cfaces = segs[i].GetNumOfFaces();
        }
    }
    return ((segs.size())?segs[index]:Segment());
}

void FindPathTo(int vertex, const Graph& graph, vector<bool>& used, vector<int>& path, int fin, vector<int>& walk){
    used[vertex] = true;
    walk.push_back(vertex);
    vector<int> neighbours = graph.GetNeighbours(vertex);
    for(int i = 0; i < neighbours.size(); ++i){
            if(!used[neighbours[i]]){
                if(neighbours[i] == fin){
                    path = walk;
                    path.push_back(fin);
                }
                FindPathTo(neighbours[i], graph, used, path, fin, walk);
            }
    }
    walk.pop_back();
}

void InsertInFace(vector<vector<int>>& faces, Segment& seg){
    int ownFace = seg.CountFaces(faces);
    swap(faces[ownFace], faces[faces.size() - 1]);
    vector<int> divFace = faces.back();
    faces.pop_back();
    vector<bool> used(MAXN, false);
    vector<int> newFace;
    vector<int> walk;
    int contVert1 = seg.GetPairOfContactVertices().first;
    int contVert2 = seg.GetPairOfContactVertices().second;
    FindPathTo(contVert1, seg, used, newFace, contVert2, walk);
    vector<int> secondFace = newFace;
    int posOfFirst = 0;
    int posOfSecond = 0;
    for(int i = 0; i < divFace.size(); ++i){
        if(divFace[i] == contVert1){
            posOfFirst = i;
        }
        if(divFace[i] == contVert2){
            posOfSecond = i;
        }
    }
    int it = (posOfSecond + 1) % divFace.size();
    while(it != posOfFirst){
        newFace.push_back(divFace[it]);
        it = (it + 1) % divFace.size();
    }
    it = (posOfSecond + divFace.size() - 1) % divFace.size();
    while(it != posOfFirst){
        secondFace.push_back(divFace[it]);
        it = (it + divFace.size() - 1) % divFace.size();
    }
    faces.push_back(newFace);
    faces.push_back(secondFace);
}

bool IsCWBPlanar(const Graph& graph){
    vector<int> cycle;
    if(!FindCycles(graph, cycle)){
        return true;
    }
    vector<vector<bool>> matrix = graph.GetMatrix();
    vector<vector<int>> faces;
    faces.push_back(cycle);
    faces.push_back(cycle);
    vector<Segment> edgeSegs;
    BuildEdgesSegments(graph, matrix, faces, edgeSegs);
    vector<Segment> segs;
    BuildSegments(graph, faces, segs);
    while(!segs.empty() || !edgeSegs.empty()){
        Segment minSeg = MinNumFaces(faces, segs);
        Segment minEdgeSeg = MinNumFaces(faces, edgeSegs);
        Segment cur;
        if(minSeg.GetNumOfFaces() > minEdgeSeg.GetNumOfFaces()){
            cur = minEdgeSeg;
        }
        else{
            cur = minSeg;
        }
        if(cur.GetNumOfFaces() == 0){
            return false;
        }
        InsertInFace(faces, cur);
        BuildEdgesSegments(graph, matrix, faces, edgeSegs);
        BuildSegments(graph, faces, segs);
    }
    return true;
}

bool IsPlanar(const Graph& graph){
    vector<Graph> comp = DivideByComponents(graph);
    vector<pair<int, int>> bridges;
    vector<Graph> finComp;
    for(int i = 0; i < comp.size(); ++i){
        FindBridgesInCG(comp[i], bridges);
        RemoveBridges(comp[i], bridges);
    }
    for(int i = 0; i < comp.size(); ++i){
        vector<Graph> buffer = DivideByComponents(comp[i]);
        for(int j = 0; j < buffer.size(); ++j){
            finComp.push_back(buffer[j]);
        }
    }
    bool isPlanar = true;
    for(int i = 0; i < finComp.size(); ++i){
        isPlanar = IsCWBPlanar(finComp[i]);
        if(!isPlanar){
            return false;
        }
    }
    return isPlanar;
}

int main(){
    int n;
    cin >> n;
    int m;
    cin >> m;
    Graph g(n);
    for(int i = 0; i < m; ++i){
        int a = 0;
        int b = 0;
        cin >> a >> b;
        g.AddEdge(a, b);
    }
    if(IsPlanar(g)){
        cout << "YES";
    }
    else{
        cout << "NO";
    }
    return 0;
}
