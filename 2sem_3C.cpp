/*Вам дан неориентированный граф, состоящий из n вершин.
На каждой вершине записано число; число, записанное на вершине i, равно ai. Изначально в графе нет ни одного ребра.
Вы можете добавлять ребра в граф за определенную стоимость.
За добавление ребра между вершинами x и y надо заплатить ax + ay монет.
Также существует m специальных предложений, каждое из которых характеризуется тремя числами x, y и w, и означает
 что можно добавить ребро между вершинами x и y за w монет. Эти специальные предложения не обязательно использовать:
 если существует такая пара вершин x и y, такая, что для нее существует специальное предложение,
 можно все равно добавить ребро между ними за ax + ay монет.
Сколько монет минимально вам потребуется, чтобы сделать граф связным?
Граф является связным, если от каждой вершины можно добраться до любой другой вершины, используя только ребра этого графа.*/
#include <iostream>
#include <vector>
#include <algorithm>
#include <limits>

using namespace std;

class Edge{
public:
    Edge(size_t _first, size_t _second, long long _cost): first(_first), second(_second), cost(_cost){}
    bool operator<(const Edge& another){return cost < another.cost;}
    bool operator==(const Edge& another){return cost == another.cost;}
    size_t GetFirst() const{return first;}
    size_t GetSecond() const{return second;}
    long long GetCost() const{return cost;}
private:
    size_t first;
    size_t second;
    long long cost;
};

class DSU{
public:
    DSU(size_t n): size(n, 1){
        for(size_t i = 0; i < n; ++i){
            parent.push_back(i);
        }
    }
    size_t GetParent(size_t v);
    void Unite(size_t v1, size_t v2);
private:
    vector<size_t> parent;
    vector<size_t> size;
};

size_t DSU::GetParent(size_t v){
    if(parent[v] == v){
        return v;
    }
    size_t x = GetParent(parent[v]);
    parent[v] = x;
    return x;
}

void DSU::Unite(size_t p1, size_t p2){
    if(p1 == p2){
        return;
    }
    if(size[p1] > size[p2]){
        swap(p1, p2);
    }
    parent[p1] = p2;
    size[p2] += size[p1];
}

long long FindWeightOfMST(vector<Edge>& edges, size_t sz){
    sort(edges.begin(), edges.end());
    DSU forest(sz);
    long long sum = 0;
    size_t cnt = 0;
    for(size_t i = 0; i < edges.size(); ++i){
        size_t f = edges[i].GetFirst(), s = edges[i].GetSecond();
        size_t p1 = forest.GetParent(f), p2 = forest.GetParent(s);
        if(p1 == p2){
            continue;
        }
        ++cnt;
        sum += edges[i].GetCost();
        if(cnt == sz - 1){
            break;
        }
        forest.Unite(p1, p2);
    }
    return sum;
}

int main(){
    size_t n, m;
    cin >> n >> m;
    vector<long long> a;
    long long minim = numeric_limits<long long>::max();
    size_t minInd = numeric_limits<size_t>::max();
    for(size_t i = 0; i < n; ++i){
        long long x;
        cin >> x;
        if(x < minim){
            minim = x;
            minInd = i;
        }
        a.push_back(x);
    }
    vector<Edge> edges;
    for(size_t i = 0; i < a.size(); ++i){
        if(i == minInd){
            continue;
        }
        edges.emplace_back(i, minInd, a[i] + a[minInd]);
    }
    for(size_t i = 0; i < m; ++i){
        size_t x, y;
        long long w;
        cin >> x >> y >> w;
        edges.emplace_back(x - 1, y - 1, w);
    }
    cout << FindWeightOfMST(edges, n);
    return 0;
}
