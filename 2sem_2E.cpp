/*Рик решил на день почувствовать себя бизнесменом!

В городе есть несколько обменников валюты.
В рамках данной задачи считаем, что каждый обменник специализируется только на двух валютах и может производить операции только с ними.
Возможно, существуют обменники, специализирующиеся на одинаковых парах валют.
В каждом обменнике — свой обменный курс: курс обмена A на B — это количество единиц валюты B, выдаваемое за 1 единицу валюты A.
Также в каждом обменнике есть комиссия — сумма, которую вы должны заплатить, чтобы производить операцию.
Комиссия взимается в той валюте, которую меняет клиент.

Например, если вы хотите поменять 100 долларов США на русские рубли в обменнике, где курс обмена равен 29.75,
а комиссия равна 0.39, вы получите (100 - 0.39) ⋅ 29.75 = 2963.3975 рублей (эх, были времена).

Вы точно знаете, что в городе используется всего N валют. Пронумеруем их числами 1, 2, …, N.
Тогда каждый обменник представляют 6 чисел:
целые A и B — номера обмениваемых валют,
а также вещественные RAB, CAB, RBA и CBA — обменные курсы и комиссии при переводе из A в B и из B в A соответственно.

Рик обладает некоторой суммой в валюте S.
Он задаётся вопросом, можно ли, после нескольких операций обмена увеличить свой капитал.
Конечно, он хочет, чтобы в конце его деньги вновь были в валюте S. Помогите ему ответить на его вопрос.
Рик должен всегда должен иметь неотрицательную сумму денег.*/
#include <iostream>
#include <vector>

using namespace std;

struct Edge{
    Edge(size_t _second, double _rate, double _comission): second(_second), rate(_rate), comission(_comission){}
    size_t second;
    double rate;
    double comission;
};

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    void AddEdge(size_t first, size_t second, double rate, double comission);
    size_t size() const;
    void GetNeighbours(size_t vertex, vector<Edge>& neighbours) const;
private:
    vector<vector<Edge>> edges;
};

void Graph::AddEdge(size_t first, size_t second, double rate, double comission){
    edges[first].emplace_back(second, rate, comission);
}

size_t Graph::size() const{
    return edges.size();
}

void Graph::GetNeighbours(size_t vertex, vector<Edge>& neighbours) const{
    neighbours = edges[vertex];
}

const size_t firstNumOfIter = 4;
const size_t secondNumOfIter = 8;

bool CanBeIncreased(const Graph& g, size_t start, double sum){
    vector<vector<double>> value(g.size(), vector<double>(secondNumOfIter * g.size(), 0.0));
    for(size_t i = 0; i < value[0].size(); ++i){
        value[start][i] = sum;
    }
    for(size_t i = 1; i < firstNumOfIter * g.size(); ++i){
        for(size_t j = 0; j < g.size(); ++j){
            vector<Edge> neighbours;
            g.GetNeighbours(j, neighbours);
            for(size_t t = 0; t < neighbours.size(); ++t){
                value[neighbours[t].second][i] = max(max(value[neighbours[t].second][i], value[neighbours[t].second][i - 1]),
                                                     (value[j][i - 1] - neighbours[t].comission) * neighbours[t].rate);
            }
        }
    }
    vector<vector<double>> fixValue = value;
    for(size_t i = firstNumOfIter * g.size(); i < secondNumOfIter * g.size(); ++i){
        for(size_t j = 0; j < g.size(); ++j){
            vector<Edge> neighbours;
            g.GetNeighbours(j, neighbours);
            for(size_t t = 0; t < neighbours.size(); ++t){
                value[neighbours[t].second][i] = max(max(value[neighbours[t].second][i], value[neighbours[t].second][i - 1]),
                                                     (value[j][i - 1] - neighbours[t].comission) * neighbours[t].rate);
            }
        }
    }
    for(size_t i = 0; i < g.size(); ++i){
        if(value[i].back() > fixValue[i][firstNumOfIter * g.size() - 1]){
            return true;
        }
    }
    return false;
}

int main(){
    size_t n, m, s;
    double v;
    cin >> n >> m >> s >> v;
    Graph g(n);
    for(size_t i = 0; i < m; ++i){
        size_t si, fi;
        double ri, ci;
        cin >> si >> fi >> ri >> ci;
        g.AddEdge(si - 1, fi - 1, ri, ci);
        cin >> ri >> ci;
        g.AddEdge(fi - 1, si - 1, ri, ci);
    }
    cout << (CanBeIncreased(g, s - 1, v)?"YES":"NO");
    return 0;
}
