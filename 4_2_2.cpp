#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

struct Request{
    int start = 0;
    int end = 0;
    Request(int _start, int _end) : start(_start), end(_end){};
    Request() = default;
};

bool Compare(const Request& firstReq, const Request& secondReq){
    if(secondReq.end >= firstReq.end){
        return true;
    }
    return false;
}

bool NonConflict(const Request& firstReq, const Request& secondReq){
    if(secondReq.start >= firstReq.end){
        return true;
    }
    if(secondReq.end <= firstReq.start){
        return true;
    }
    return false;
}

void ChooseRequests(const vector<Request>& requests, vector<Request>& used){
    used.push_back(requests[0]);
    for(int i = 1; i < requests.size(); ++i){
        if(NonConflict(requests[i], used.back())){
            used.push_back(requests[i]);
        }
    }
}

int main(){
    int a, b;
    vector<Request> requests;
    while(cin >> a && cin >> b){
        requests.emplace_back(a, b);
    }
    sort(requests.begin(), requests.end(), Compare);
    vector<Request> used;
    ChooseRequests(requests, used);
    cout << used.size();
    return 0;
}
