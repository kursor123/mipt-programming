using namespace std;

template<typename T>
class List{
public:
    struct Node{
        Node() = default;
        Node(const T& _value): value(new T(_value)){}
        Node(T&& _value): value(new T(move(_value))){}
        ~Node(){delete value;}
        template<typename ...Args>
        Node(const Args&... args): value(new T(args...)){}
        template<typename ...Args>
        Node(Args&&... args): value(new T(move(args)...)){}
        Node* prev = nullptr;
        Node* next = nullptr;
        T* value = nullptr;
    };
    template<bool Const = false>
    struct Iterator{
        using value_type = T;
        using reference = conditional_t<Const, const T&, T&>;
        using pointer = conditional_t<Const, const T*, T*>;
        using difference_type = size_t;
        using iterator_category = bidirectional_iterator_tag;
        using ConditionalNode = conditional_t<Const, const Node, Node>;
        friend class List<T>;
        reference operator*(){return *(ptr->value);}
        reference operator*() const{return *(ptr->value);}
        Iterator& operator++(){ptr = ptr->next; return *this;}
        Iterator& operator++(int){Iterator tmp(ptr); ptr = ptr->next; return tmp;}
        Iterator& operator--(){ptr = ptr->prev; return *this;}
        Iterator& operator--(int){Iterator tmp(ptr); ptr = ptr->prev; return tmp;}
        pointer operator->(){return ptr->value;}
        bool operator!=(const Iterator& another) const {return ptr != another.ptr;}
        bool operator==(const Iterator& another) const {return ptr == another.ptr;}
        operator Iterator<true>(){return Iterator<true>(ptr);}
        Node* GetPtr(){return const_cast<Node*>(ptr);}
    private:
        Iterator(ConditionalNode* _ptr): ptr(_ptr){}
        ConditionalNode* ptr;
    };

    using value_type = T;
    using iterator = Iterator<false>;
    using const_iterator = Iterator<true>;

    List();
    List(size_t count, const T& value = T());
    List(const List<T>& another);
    List(List<T>&& another);
    ~List();

    List& operator=(const List<T>& another);
    List& operator=(List<T>&& another);
    size_t size() const;
    T& front();
    T& back();
    void clear();
    bool empty() const;
    void push_back(T&& value);
    void push_back(const T& value);
    void pop_back();
    void push_front(T&& value);
    void push_front(const T& value);
    void pop_front();
    auto begin(){return Iterator<false>(head);}
    auto end(){return Iterator<false>(fictive);}
    auto cbegin() const{return Iterator<true>(head);}
    auto cend() const{return Iterator<true>(fictive);}
    iterator insert(const_iterator it, const T& value);
    iterator insert(const_iterator it, T&& value);
    template<typename InputIter>
    iterator insert(const_iterator it, InputIter first, InputIter last);
    iterator erase(const_iterator it);
    iterator erase(iterator first, iterator last);
    template<typename ...Args>
    iterator emplace(const_iterator pos, const Args&... args);
    template<typename ...Args>
    iterator emplace(const_iterator pos, Args&&... args);
    template<typename ...Args>
    iterator emplace_back(const Args&... args){return emplace(cend(), args...);}
    template<typename ...Args>
    iterator emplace_back(Args&&... args){return emplace(cend(), move(args)...);}
    template<typename ...Args>
    iterator emplace_front(const Args&... args){return emplace(cbegin(), args...);}
    template<typename ...Args>
    iterator emplace_front(Args&&... args){return emplace(cbegin(), move(args)...);}
    void reverse();
    void unique();
private:
    Node* fictive = new Node();
    Node* head = fictive;
    Node* tail = fictive;
    size_t cnt = 0;
};

template<typename T>
List<T>::List(){
    fictive->next = fictive->prev = fictive;
}

template<typename T>
List<T>::List(size_t count, const T& value): cnt(count){
    head = new Node(value);
    head->prev = fictive;
    Node* tmp = head;
    for(size_t i = 1; i < cnt; ++i){
        tmp->next = new Node(value);
        tmp->next->prev = tmp;
        tmp = tmp->next;
    }
    tail = tmp;
    tail->next = fictive;
    fictive->prev = tail;
    fictive->next = head;
}

template<typename T>
List<T>::List(const List<T>& another): cnt(another.cnt){
    if(!cnt){
        return;
    }
    head = new Node(*(another.head->value));
    Node* it = head;
    Node* anotherIt = another.head;
    for(size_t i = 1; i < cnt; ++i){
        it->next = new Node(*(anotherIt->next->value));
        it->next->prev = it;
        it = it->next;
        anotherIt = anotherIt->next;
    }
    tail = it;
    tail->next = fictive;
    head->prev = fictive;
    fictive->next = head;
    fictive->prev = tail;
}

template<typename T>
List<T>::List(List<T>&& another): fictive(another.fictive), head(another.head), tail(another.tail), cnt(another.cnt){
    another.fictive = nullptr;
    another.head = nullptr;
    another.tail = nullptr;
    another.cnt = 0;
}

template<typename T>
void List<T>::clear(){
    cnt = 0;
    Node* it = head;
    while(it != fictive){
        Node* tmp = it->next;
        delete it;
        it = tmp;
    }
    head = tail = fictive;
    if(fictive){
   	fictive->next = fictive->prev = fictive;
    }
}

template<typename T>
List<T>::~List(){
    clear();
    delete fictive;
}

template<typename T>
List<T>& List<T>::operator=(const List<T>& another){
    clear();
    cnt = another.cnt;
    if(!cnt){
        return *this;
    }
    head = new Node(*(another.head->value));
    Node* it = head;
    Node* anotherIt = another.head;
    for(size_t i = 1; i < cnt; ++i){
        it->next = new Node(*(anotherIt->next->value));
        it->next->prev = it;
        it = it->next;
        anotherIt = anotherIt->next;
    }
    tail = it;
    tail->next = fictive;
    head->prev = fictive;
    fictive->next = head;
    fictive->prev = tail;
    return *this;
}

template<typename T>
List<T>& List<T>::operator=(List<T>&& another){
    clear();
    delete fictive;
    fictive = another.fictive;
    head = another.head;
    tail = another.tail;
    cnt = another.cnt;
    another.fictive = nullptr;
    another.head = nullptr;
    another.tail = nullptr;
    another.cnt = 0;
    return *this;
}

template<typename T>
size_t List<T>::size() const{
    return cnt;
}

template<typename T>
T& List<T>::front(){
    return *(head->value);
}

template<typename T>
T& List<T>::back(){
    return *(tail->value);
}

template<typename T>
bool List<T>::empty() const{
    return (cnt == 0);
}

template<typename T>
void List<T>::push_back(T&& value){
    Node* newNode = new Node(move(value));
    if(cnt){
        tail->next = newNode;
        newNode->prev = tail;
        newNode->next = fictive;
        fictive->prev = newNode;
        tail = newNode;
        cnt++;
        return;
    }
    head = newNode;
    tail = newNode;
    cnt = 1;
    head->next = fictive;
    head->prev = fictive;
    fictive->prev = head;
    fictive->next = head;
}

template<typename T>
void List<T>::push_back(const T& value){
    Node* newNode = new Node(value);
    if(cnt){
        tail->next = newNode;
        newNode->prev = tail;
        newNode->next = fictive;
        fictive->prev = newNode;
        tail = newNode;
        cnt++;
        return;
    }
    head = newNode;
    tail = newNode;
    cnt = 1;
    tail->next = fictive;
    head->prev = fictive;
    fictive->prev = head;
    fictive->next = head;
}

template<typename T>
void List<T>::pop_back(){
    if(empty()){
        return;
    }
    if(cnt == 1){
        clear();
        return;
    }
    Node* tmp = tail;
    tail = tail->prev;
    tail->next = fictive;
    fictive->prev = tail;
    cnt--;
    delete tmp;
}

template<typename T>
void List<T>::push_front(T&& value){
    Node* newNode = new Node(move(value));
    if(cnt){
        head->prev = newNode;
        newNode->prev = fictive;
        newNode->next = head;
        fictive->next = newNode;
        head = newNode;
        cnt++;
        return;
    }
    head = newNode;
    tail = newNode;
    cnt = 1;
    fictive = new Node(move(value));
    head->next = fictive;
    head->prev = fictive;
    fictive->prev = head;
    fictive->next = head;
}

template<typename T>
void List<T>::push_front(const T& value){
    Node* newNode = new Node(value);
    if(cnt){
        head->prev = newNode;
        newNode->prev = fictive;
        newNode->next = head;
        fictive->next = newNode;
        head = newNode;
        cnt++;
        return;
    }
    head = newNode;
    tail = newNode;
    cnt = 1;
    fictive = new Node(value);
    head->next = fictive;
    head->prev = fictive;
    fictive->prev = head;
    fictive->next = head;
}

template<typename T>
void List<T>::pop_front(){
    if(empty()){
        return;
    }
    if(cnt == 1){
        clear();
        return;
    }
    Node* tmp = head;
    head = head->next;
    head->prev = fictive;
    fictive->next = head;
    cnt--;
    delete tmp;
}

template<typename T>
typename List<T>::iterator List<T>::insert(const_iterator it, const T& value){
    Node* newNode = new Node(value);
    if(it == cbegin()){
        head = newNode;
    }
    if(it == cend()){
        tail = newNode;
    }
    newNode->next = it.GetPtr();
    newNode->prev = it.GetPtr()->prev;
    newNode->next->prev = newNode;
    newNode->prev->next = newNode;
    cnt++;
    return iterator(newNode);
}

template<typename T>
typename List<T>::iterator List<T>::insert(const_iterator it, T&& value){
    Node* newNode = new Node(move(value));
    if(it == cbegin()){
        head = newNode;
    }
    if(it == cend()){
        tail = newNode;
    }
    newNode->next = it.GetPtr();
    newNode->prev = it.GetPtr()->prev;
    newNode->next->prev = newNode;
    newNode->prev->next = newNode;
    cnt++;
    return iterator(newNode);
}

template<typename T>
template<typename InputIter>
typename List<T>::iterator List<T>::insert(const_iterator it, InputIter first, InputIter last){
    Node* tmp = it->ptr->prev;
    Node* start = tmp;
    size_t count = 0;
    for(auto it = first; it != last; ++it){
        count++;
        tmp->next = new Node(*it);
        tmp->next->prev = tmp;
        tmp = tmp->next;
    }
    cnt += count;
    tmp->next = it->ptr;
    it->ptr->prev = tmp;
    head = fictive->next;
    tail = fictive->prev;
    return iterator(start->next);
}

template<typename T>
typename List<T>::iterator List<T>::erase(const_iterator it){
    bool isHead = it.ptr == head, isTail = it.ptr == tail;
    if(size() == 1){
        clear();
        return end();
    }
    --cnt;
    it.GetPtr()->next->prev = it.GetPtr()->prev;
    it.GetPtr()->prev->next = it.GetPtr()->next;
    iterator retVal(it.GetPtr()->next);
    if(isHead){
        head = it.GetPtr()->next;
    }
    if(isTail){
        tail = it.GetPtr()->prev;
    }
    delete it.GetPtr();
    return retVal;
}

template<typename T>
typename List<T>::iterator List<T>::erase(iterator first, iterator last){
    if(first == last){
        return last;
    }
    iterator tmp = first;
    ++first;
    for(auto it = first; it != last; ++it){
        erase(tmp);
        tmp = it;
    }
    erase(tmp);
    return last;
}

template<typename T>
template<typename ...Args>
typename List<T>::iterator List<T>::emplace(const_iterator pos, const Args&... args){
    Node* newNode = new Node(args...);
    newNode->next = pos.GetPtr();
    newNode->prev = pos.GetPtr()->prev;
    newNode->next->prev = newNode;
    newNode->prev->next = newNode;
    head = fictive->next;
    tail = fictive->prev;
    cnt++;
    return iterator(newNode);
}


template<typename T>
template<typename ...Args>
typename List<T>::iterator List<T>::emplace(const_iterator pos, Args&&... args){
    Node* newNode = new Node(move(args)...);
    newNode->next = pos.GetPtr();
    newNode->prev = pos.GetPtr()->prev;
    newNode->next->prev = newNode;
    newNode->prev->next = newNode;
    head = fictive->next;
    tail = fictive->prev;
    cnt++;
    return iterator(newNode);
}

template<typename T>
void List<T>::reverse(){
    Node* tmp = head;
    while(true){
        swap(tmp->next, tmp->prev);
        if(tmp == fictive){
            break;
        }
        tmp = tmp->prev;
    }
    swap(head, tail);
}

template<typename T>
void List<T>::unique(){
    iterator start = begin();
    for(auto it = begin(); it != end(); ++it){
        if(!(*it == *start)){
            erase(++start, it);
            start = it;
        }
    }
    erase(++start, end());
}
