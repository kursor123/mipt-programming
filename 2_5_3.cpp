#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>

using namespace std;

int8_t FindBinDigit(uint64_t n, int8_t bit){
        return (n >> bit) % 2;
}

int8_t FindMaxBit(uint64_t n){
    int8_t left = 0;
    int8_t right = 63;
    while(left < right){
        if(n > pow(2, (left + right) / 2)){
            left = (left + right) / 2;
        }
        else{
            right = (left + right) / 2;
        }
        if(right - left == 1){
            if(n < pow(2, right)){
                return left;
            }
            else{
                return right;
            }
        }
    }
    return left;
}

int32_t BinPartition(vector<uint64_t>& arr, int32_t left, int32_t right, int8_t bit){
    if(bit < 0){
        return -1;
    }
    int32_t i = left;
    int32_t j = right;
    while(i < j){
        while((i <= right) &&(FindBinDigit(arr[i], bit) == 0)){
            i++;
        }
        while((j >= left)&&(FindBinDigit(arr[j], bit) == 1)){
            j--;
        }
        if(i < j){
            swap(arr[i], arr[j]);
        }
    }
    return j;
}

void BinRadixSort(vector<uint64_t>& arr, int32_t left, int32_t right, int8_t bit){
    if(left == right){
        return;
    }
    if(bit < 0){
        return;
    }
    int32_t border = BinPartition(arr, left, right, bit);
    if((border == (left - 1)) || (border == right)){
        BinRadixSort(arr, left, right, bit - 1);
    }
    else{
        BinRadixSort(arr, left, border, bit - 1);
        BinRadixSort(arr, border + 1, right, bit - 1);
    }
}

int main(){
    int32_t n;
    cin >> n;
    vector<uint64_t> arr(n);
    int8_t maxBit = 0;
    for(int32_t i = 0; i < n; ++i){
        cin >> arr[i];
        if(FindMaxBit(arr[i]) > maxBit){
            maxBit = FindMaxBit(arr[i]);
        }
    }

    BinRadixSort(arr, 0, n - 1, maxBit);
    for(int32_t i = 0; i < n; ++i){
        cout << arr[i] << ' ';
    }
    return 0;
}
