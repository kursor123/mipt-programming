/*Реализуйте структуру данных “массив строк” на основе декартового дерева по неявному ключу со следующими методами:
    // Добавление строки в позицию position.\\
    // Все последующие строки сдвигаются на одну позицию вперед.\\
    void InsertAt( int position, const std::string& value );\\
    // Удаление строки из позиции position.\\
    // Все последующие строки сдвигаются на одну позицию назад.\\
    void DeleteAt( int position );\\
    // Получение строки из позиции position.\\
    std::string GetAt( int position );
Все методы должны работать за O(log n) в среднем, где n – текущее количество строк в массиве.*/
#include <iostream>
#include <vector>
#include <random>
#include <limits>
#include <memory>

using namespace std;

default_random_engine generator;
uniform_int_distribution<int> distribution(1, numeric_limits<int>::max());

struct Node{
    Node(const string& _data, int _prior): data(_data), prior(_prior){}
    ~Node() = default;
    string data;
    int prior;
    shared_ptr<Node> lchild = nullptr;
    shared_ptr<Node> rchild = nullptr;
    size_t size = 1;
};

size_t NodeSize(const shared_ptr<Node> ptr){
    return (ptr != nullptr)?(ptr->size):0;
}

class CartesianTree{
public:
    CartesianTree() = default;
    void InsertAt(size_t position, const string& value);
    void DeleteFromTo(size_t from, size_t to);
    string GetAt(size_t position);
private:
    pair<shared_ptr<Node>, shared_ptr<Node>> Split(shared_ptr<Node> tree, size_t key);
    shared_ptr<Node> Merge(shared_ptr<Node> lTree, shared_ptr<Node> rTree);
    shared_ptr<Node> root = nullptr;
};

shared_ptr<Node> CartesianTree::Merge(shared_ptr<Node> lTree, shared_ptr<Node> rTree){
    if(lTree == nullptr){
        return rTree;
    }
    if(rTree == nullptr){
        return lTree;
    }
    if(lTree->prior < rTree->prior){
        lTree->size -= NodeSize(lTree->rchild);
        shared_ptr<Node> res = Merge(lTree->rchild, rTree);
        lTree->rchild = res;
        lTree->size += NodeSize(lTree->rchild);
        return lTree;
    }
    else{
        rTree->size -= NodeSize(rTree->lchild);
        shared_ptr<Node> res = Merge(lTree, rTree->lchild);
        rTree->lchild = res;
        rTree->size += NodeSize(rTree->lchild);
        return rTree;
    }
}

pair<shared_ptr<Node>, shared_ptr<Node>> CartesianTree::Split(shared_ptr<Node> tree, size_t key){
    if(tree == nullptr){
        return make_pair(nullptr, nullptr);
    }
    if(NodeSize(tree->lchild) == key){
        auto res = make_pair(tree->lchild, tree);
        tree->size -= NodeSize(tree->lchild);
        tree->lchild = nullptr;
        return res;
    }
    if(NodeSize(tree->lchild) < key){
        tree->size -= NodeSize(tree->rchild);
        auto res = Split(tree->rchild, key - 1 - NodeSize(tree->lchild));
        tree->rchild = res.first;
        tree->size += NodeSize(tree->rchild);
        return make_pair(tree, res.second);
    }
    else{
        tree->size -= NodeSize(tree->lchild);
        auto res = Split(tree->lchild, key);
        tree->lchild = res.second;
        tree->size += NodeSize(tree->lchild);
        return make_pair(res.first, tree);
    }
}

void CartesianTree::InsertAt(size_t position, const string& value){
    shared_ptr<Node> newNode = make_shared<Node>(value, distribution(generator));
    if(position == 0 && root == nullptr){
        root = newNode;
        return;
    }
    auto res = Split(root, position);
    auto x = Merge(res.first, newNode);
    root = Merge(x, res.second);
}

void CartesianTree::DeleteFromTo(size_t from, size_t to){
    auto res1 = Split(root, to + 1);
    auto res2 = Split(res1.first, from);
    root = Merge(res2.first, res1.second);
}

string CartesianTree::GetAt(size_t position){
    auto res1 = Split(root, position + 1);
    auto res2 = Split(res1.first, position);
    string str = res2.second->data;
    root = Merge(Merge(res2.first, res2.second), res1.second);
    return str;
}

int main(){
    CartesianTree tree;
    int n;
    cin >> n;
    for(size_t i = 0; i < n; ++i){
        char command;
        cin >> command;
        size_t position;
        cin >> position;
        if(command == '+'){
            string newStr;
            cin >> newStr;
            tree.InsertAt(position, newStr);
        }
        if(command == '?'){
            cout << tree.GetAt(position) << endl;
        }
        if(command == '-'){
            size_t secondPosition;
            cin >> secondPosition;
            tree.DeleteFromTo(position, secondPosition);
        }
    }
    return 0;
}
