/*Рик отправляет Морти в путешествие по N вселенным.
У него есть список всех существующих однонаправленных телепортов.
Чтобы Морти не потерялся, Рику необходимо узнать,
между какими вселенными существуют пути, а между какими нет. Помогите ему в этом!*/
#include <iostream>
#include <vector>

using namespace std;

struct Edge{
    Edge(size_t _second, double _cost): second(_second), cost(_cost){}
    size_t second;
    double cost;
};

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    void AddEdge(size_t first, size_t second, double cost);
    size_t Size() const;
    void GetNeighbours(size_t vertex, vector<Edge>& neighbours) const;
private:
    vector<vector<Edge>> edges;
};

void Graph::AddEdge(size_t first, size_t second, double cost){
    edges[first].emplace_back(second, cost);
    edges[second].emplace_back(first, cost);
}

size_t Graph::Size() const{
    return edges.size();
}

void Graph::GetNeighbours(size_t vertex, vector<Edge>& neighbours) const{
    neighbours = edges[vertex];
}

const size_t sz = 32;

unsigned int Bit(vector<vector<unsigned int>>& matrix, size_t i, size_t k){
    return ((matrix[i][k / sz] >> (sz - 1 - k % sz)) & 1);
}

void FindAccessibleVertices(vector<vector<unsigned int>>& matrix){
    for(size_t k = 0; k < matrix.size(); ++k){
        for(size_t i = 0; i < matrix.size(); ++i){
            if(Bit(matrix, i, k)){
                for(size_t j = 0; j < matrix[0].size(); ++j){
                    matrix[i][j] |= matrix[k][j];
                }
            }
        }
    }
}

int main(){
    size_t n;
    cin >> n;
    vector<vector<unsigned int>> matrix(n, vector<unsigned int>(n / sz + 1));
    for(size_t i = 0; i < n; ++i){
        string row;
        cin >> row;
        for(size_t j = 0; j < n; ++j){
            char c = row[j];
            matrix[i][j / sz] |= atoi(&c) << (sz - 1 - j % sz);
        }
    }
    FindAccessibleVertices(matrix);
    for(size_t i = 0; i < n; ++i){
        for(size_t j = 0; j < n; ++j){
            cout << Bit(matrix, i, j);
        }
        cout << endl;
    }
    return 0;
}
