/*��� "����� �������� ������" �������� ��� ����������� ������� ������������ ������� �� ����� ������� ������ �������� ����������.
������� �������� ����� N �������, �������������� �� 0 �� N-1. ����������� ������ ����������.
� ������� ������������ ��������������� �������� ������� �� ���������� � ��������� ������ ��������� � �������� �������,
� ����� ���������� �������, ������� �������� ����� ����������.
��������� �������� ��������� ������� ������� - O(n log n).*/
#include <iostream>
#include <vector>
#include <limits>

using namespace std;

class RangeTree{
public:
    RangeTree(const vector<int>& arr);
    int RangeMinimumQuery(size_t left, size_t right);
    void Add(size_t left, size_t right, int val);
private:
    int InnerRMQ(size_t vertex, size_t left, size_t right, size_t ctrlLeft, size_t ctrlRight);
    void InnerAdd(size_t vertex, size_t left, size_t right, int val, size_t ctrlLeft, size_t ctrlRight);
    void Push(size_t vertex);
    vector<int> add;
    vector<int> tree;
};

RangeTree::RangeTree(const vector<int>& arr): add(arr.size()), tree(arr.size()){
    size_t size = 1;
    while(size < arr.size()){
        size <<= 1;
    }
    while(add.size() < 2 * size - 1){
        add.push_back(0);
        tree.push_back(0);
    }
    size_t ind = 0;
    for(; ind < arr.size(); ++ind){
        tree[size - 1 + ind] = arr[ind];
    }
    while(size - 1 + ind < tree.size()){
        tree[size - 1 + ind] = numeric_limits<int>::max();
        ind++;
    }
    for(int i = size - 2; i >= 0; --i){
        tree[i] = min(tree[2 * i + 1], tree[2 * i + 2]);
    }
}

int RangeTree::InnerRMQ(size_t vertex, size_t left, size_t right, size_t ctrlLeft, size_t ctrlRight){
    Push(vertex);
    if(left == ctrlLeft && right == ctrlRight){
        return tree[vertex];
    }
    size_t middle = (ctrlLeft + ctrlRight) / 2;
    if(right <= middle){
        return InnerRMQ(2 * vertex + 1, left, right, ctrlLeft, middle);
    }
    if(left > middle){
        return InnerRMQ(2 * vertex + 2, left, right, middle + 1, ctrlRight);
    }
    return min(InnerRMQ(2 * vertex + 1, left, middle, ctrlLeft, middle),
               InnerRMQ(2 * vertex + 2, middle + 1, right, middle + 1, ctrlRight));
}

int RangeTree::RangeMinimumQuery(size_t left, size_t right){
    return InnerRMQ(0, left, right, 0, tree.size() / 2);
}

void RangeTree::InnerAdd(size_t vertex, size_t left, size_t right, int val, size_t ctrlLeft, size_t ctrlRight){
    Push(vertex);
    if(left == ctrlLeft && right == ctrlRight){
        tree[vertex] += val;
        add[vertex] += val;
        return;
    }
    size_t middle = (ctrlLeft + ctrlRight) / 2;
    if(right <= middle){
        InnerAdd(2 * vertex + 1, left, right, val, ctrlLeft, middle);
        tree[vertex] = min(tree[2 * vertex + 1], tree[2 * vertex + 2]);
        return;
    }
    if(left > middle){
        InnerAdd(2 * vertex + 2, left, right, val, middle + 1, ctrlRight);
        tree[vertex] = min(tree[2 * vertex + 1], tree[2 * vertex + 2]);
        return;
    }
    InnerAdd(2 * vertex + 1, left, middle, val, ctrlLeft, middle);
    InnerAdd(2 * vertex + 2, middle + 1, right,  val, middle + 1, ctrlRight);
    tree[vertex] = min(tree[2 * vertex + 1], tree[2 * vertex + 2]);
}

void RangeTree::Push(size_t vertex){
    if(add[vertex] == 0){
        return;
    }
    if(2 * vertex + 2 >= tree.size()){
        add[vertex] = 0;
        return;
    }
    for(size_t i = 1; i < 3; ++i){
        if(tree[2 * vertex + i] == numeric_limits<int>::max()){
            continue;
        }
        tree[2 * vertex + i] += add[vertex];
        add[2 * vertex + i] += add[vertex];
    }
    add[vertex] = 0;
}

void RangeTree::Add(size_t left, size_t right, int val){
    InnerAdd(0, left, right, val, 0, tree.size() / 2);
}

int main(){
    size_t n;
    cin >> n;
    vector<int> trip(n - 1);
    for(size_t i = 0; i < trip.size(); ++i){
        int initial;
        cin >> initial;
        trip[i] = initial;
    }
    int capacity;
    cin >> capacity;
    for(size_t i = 0; i < trip.size(); ++i){
        trip[i] = capacity - trip[i];
    }
    RangeTree tree(trip);
    size_t amount;
    cin >> amount;
    vector<size_t> errors;
    for(size_t i = 0; i < amount; ++i){
        size_t start, end;
        cin >> start >> end;
        int count;
        cin >> count;
        if(tree.RangeMinimumQuery(start, end - 1) < count){
            errors.push_back(i);
            continue;
        }
        tree.Add(start, end - 1, -count);
    }
    for(auto e : errors){
        cout << e << ' ';
    }
    return 0;
}
