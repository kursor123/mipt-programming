/*Фрэнку опять прилетел новый заказ.
Однако в этот раз город играет по очень странным правилам:
все дороги в городе односторонние и связывают только офисы нанимателей перевозчика.
Множество офисов, между любыми двумя из которых существует путь,
образуют квартал, если нельзя добавить никакие другие, чтобы условие выполнялось.
Фрэнку интересно, каково минимальное количество односторонних дорог нужно ещё построить, чтобы весь город стал квраталом.*/
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <queue>
#include <stack>
#include <assert.h>
using namespace std;

class Graph{
public:
    explicit Graph(size_t n): out(n){}
    ~Graph() = default;
    void AddEdge(size_t first, size_t second);
    size_t size() const;
    const vector<size_t>& GetNeighbours(size_t vertex) const;
private:
    vector<vector<size_t>> out;
};

void Graph::AddEdge(size_t first, size_t second){
    out[first].push_back(second);
}

size_t Graph::size() const{
    return out.size();
}

const vector<size_t>& Graph::GetNeighbours(size_t vertex) const{
    return out[vertex];
}

Graph Reverse(const Graph& graph){
    Graph reverted(graph.size());
    for(size_t i = 0; i < graph.size(); ++i){
        const vector<size_t>& neighbours = graph.GetNeighbours(i);
        for(int j = 0; j < neighbours.size(); ++j){
            reverted.AddEdge(neighbours[j], i);
        }
    }
    return reverted;
}

void InnerDFS(const Graph& graph, size_t vertex, vector<size_t>& time, vector<bool>& colors){
    colors[vertex] = false;
    const vector<size_t>& neighbours = graph.GetNeighbours(vertex);
    for(size_t i = 0; i < neighbours.size(); ++i){
        if(colors[neighbours[i]]){
            InnerDFS(graph, neighbours[i], time, colors);
        }
    }
    time.push_back(vertex);
}

void DFS(const Graph& graph, vector<size_t>& time){
    size_t walkIt = 0;
    vector<bool> colors(graph.size(), true);
    while(walkIt < graph.size()){
        while(walkIt < graph.size() && !colors[walkIt]){
            walkIt++;
        }
        if(walkIt < graph.size()){
            InnerDFS(graph, walkIt, time, colors);
            walkIt++;
        }
    }
}

void FindComponent(const Graph& graph, size_t vertex, vector<size_t>& components, vector<bool>& colors, size_t numberOfComponents){
    components[vertex] = numberOfComponents;
    colors[vertex] = false;
    const vector<size_t>& neighbours = graph.GetNeighbours(vertex);
    for(size_t i = 0; i < neighbours.size(); ++i){
        if(colors[neighbours[i]]){
            FindComponent(graph, neighbours[i], components, colors, numberOfComponents);
        }
    }
}

size_t GetSCC(const Graph& graph, vector<size_t>& components){
    vector<size_t> time;
    DFS(graph, time);
    size_t numberOfComponents = 0;
    int walkIt = graph.size() - 1;
    vector<bool> colors(graph.size(), true);
    Graph reverted = Reverse(graph);
    while(walkIt >= 0){
        while(walkIt >= 0 && !colors[time[walkIt]]){
            walkIt--;
        }
        if(walkIt >= 0){
            FindComponent(reverted, time[walkIt], components, colors, numberOfComponents);
            numberOfComponents++;
            walkIt--;
        }
    }
    return numberOfComponents;
}

void GetSimplifiedCondensation(const Graph& graph, const vector<size_t>& components, vector<short>& condensation){
    for(size_t i = 0; i < graph.size(); ++i){
        const vector<size_t>& neighbours = graph.GetNeighbours(i);
        for(size_t j = 0; j < neighbours.size(); ++j){
            if(components[i] != components[neighbours[j]]){
                condensation[components[i]] |= 2;
                condensation[components[neighbours[j]]] |= 1;
            }
        }
    }
}

size_t CountEdgesToSCG(const Graph& graph){
    vector<size_t> components(graph.size());
    size_t numberOfComponents = GetSCC(graph, components);
    if(numberOfComponents == 1){
        return 0;
    }
    vector<short> condensation(numberOfComponents, 0);
    GetSimplifiedCondensation(graph, components, condensation);
    size_t outNumber = 0;
    size_t inNumber = 0;
    for(size_t i = 0; i < numberOfComponents; ++i){
        outNumber += (condensation[i] == 2 || condensation[i] == 0)?1:0;
        inNumber += (condensation[i] == 1 || condensation[i] == 0)?1:0;
    }
    return max(outNumber, inNumber);
}

int main(){
    size_t n;
    cin >> n;
    Graph g(n);
    size_t m;
    cin >> m;
    size_t a = 0;
    size_t b = 0;
    for(size_t i = 0; i < m; ++i){
        cin >> a >> b;
        g.AddEdge(a - 1, b - 1);
    }
    cout << CountEdgesToSCG(g);
    return 0;
}
