/*���� � ��� ��� ���� �� ���� �����.
� ���������� ������ ���������� ���, ������� ������� �� �� ������.
�� ��� �����������, ������� �� ����� ���� ������.
��� ������������ ����� ������, ������������� ������� �� 1 �� n � ����������� m ���������
(����� ���� ��������� ������� ����������� ��� ������, ����� ���� �������, ����������� ������ � ����� ��).
��-�� �����, ���� �� ������� ������ ���� �� ������, �� ������ �� ��� �� ������� ��� ���� �� �����.
������ ������ ��������� �� ������ � ������� s, � ������ ����� � �� ������ � ������� t. �������� ����� � ���� ��������� �� ������.*/
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    ~Graph() = default;
    void AddEdge(size_t first, size_t second);
    void RemoveEdge(size_t first, size_t second);
    size_t Size() const;
    vector<size_t> GetNeighbours(size_t vertex) const;
private:
    vector<vector<size_t>> edges;
};

void Graph::AddEdge(size_t first, size_t second){
    edges[first].push_back(second);
}

void Graph::RemoveEdge(size_t first, size_t second){
    for(size_t i = 0; i < edges[first].size(); ++i){
        if(edges[first][i] == second){
            swap(edges[first][i], edges[first].back());
            edges[first].pop_back();
            break;
        }
    }
}

size_t Graph::Size() const{
    return edges.size();
}

vector<size_t> Graph::GetNeighbours(size_t vertex) const{
    return edges[vertex];
}

bool FindPathBetween(const Graph& g, vector<bool>& used, vector<size_t>& path, size_t s, size_t t){
    used[s] = true;
    path.push_back(s);
    if(s == t){
        return true;
    }
    for(size_t nb : g.GetNeighbours(s)){
        if(used[nb]){
            continue;
        }
        if(FindPathBetween(g, used, path, nb, t)){
            return true;
        }
    }
    path.pop_back();
    return false;
}

pair<vector<size_t>, vector<size_t>> FindDisjointPathes(Graph& g, size_t s, size_t t){
    //������� ������ ��� ���������������� ���� ������� �����-���������� � ������� ����������� 1.
    //����� �� ��� �������� ����, � ������� ������ ������� ���� ����, ���������, ����� ������.
    //������� ���������� ��������� ����.
    vector<bool> used(g.Size(), false);
    vector<size_t> path1;
    pair<vector<size_t>, vector<size_t>> pathes;
    if(!FindPathBetween(g, used, path1, s, t)){
        return pathes;
    }
    for(size_t i = 1; i < path1.size(); ++i){
        g.RemoveEdge(path1[i - 1], path1[i]);
        g.AddEdge(path1[i], path1[i - 1]);
    }
    used.assign(used.size(), false);
    vector<size_t> path2;
    if(!FindPathBetween(g, used, path2, s, t)){
        return pathes;
    }
    vector<int> hash1(g.Size(), -1);
    for(size_t i = 0; i < path1.size(); ++i){
        hash1[path1[i]] = i;
    }
    vector<bool> allowed1(path1.size(), true);
    vector<bool> allowed2(path2.size(), true);
    for(size_t i = 1; i < path2.size(); ++i){
        int v1 = hash1[path2[i]], v2 = hash1[path2[i - 1]];
        if(v1 != -1 && v2 != -1){
            if(v1 + 1 == v2){
                allowed1[v2] = false;
                allowed2[i] =false;
            }
        }
    }
    Graph resNet(g.Size());
    for(size_t i = 1; i < path1.size(); ++i){
        if(allowed1[i]){
            resNet.AddEdge(path1[i - 1], path1[i]);
        }
    }
    for(size_t i = 1; i < path2.size(); ++i){
        if(allowed2[i]){
            resNet.AddEdge(path2[i - 1], path2[i]);
        }
    }
    used.assign(g.Size(), false);
    FindPathBetween(resNet, used, pathes.first, s, t);
    for(size_t i = 1; i < pathes.first.size(); ++i){
        resNet.RemoveEdge(pathes.first[i - 1], pathes.first[i]);
    }
    used.assign(g.Size(), false);
    FindPathBetween(resNet, used, pathes.second, s, t);
    return pathes;
}

int main(){
    size_t n, m, s, t;
    cin >> n >> m >> s >> t;
    --s;
    --t;
    Graph g(n);
    for(size_t i = 0; i < m; ++i){
        size_t a, b;
        cin >> a >> b;
        g.AddEdge(a - 1, b - 1);
    }
    auto pathes = FindDisjointPathes(g, s, t);
    if(pathes.first.size() != 0 && pathes.second.size() != 0){
        cout << "YES" << endl;
        for(size_t i = 0; i < pathes.first.size(); ++i){
            cout << pathes.first[i] + 1 << ' ';
        }
        cout << endl;
        for(size_t i = 0; i < pathes.second.size(); ++i){
            cout << pathes.second[i] + 1 << ' ';
        }
    }
    else{
        cout << "NO";
    }
    return 0;
}
