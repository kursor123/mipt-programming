#include <iostream>
#include <vector>
#include <string>

using namespace std;

class BigInteger;

static BigInteger powerOfTen(size_t precision);

class BigInteger{
public:
    BigInteger(int _number);
    BigInteger() = default;
    explicit operator bool() const;
    const string toString(bool nul = false) const;
    BigInteger& operator+=(const BigInteger& second);
    friend const BigInteger operator+(const BigInteger& first, const BigInteger& second);
    BigInteger& operator-=(const BigInteger& second);
    friend const BigInteger operator-(const BigInteger& first, const BigInteger& second);
    BigInteger& operator*=(const BigInteger& second);
    friend const BigInteger operator*(const BigInteger& first, const BigInteger& second);
    BigInteger& operator/=(const BigInteger& second);
    friend const BigInteger operator/(const BigInteger& first, const BigInteger& second);
    BigInteger& operator%=(const BigInteger& second);
    friend const BigInteger operator%(const BigInteger& first, const BigInteger& second);
    BigInteger& operator++();
    const BigInteger operator++(int);
    BigInteger& operator--();
    const BigInteger operator--(int);
    BigInteger operator-() const;
    friend bool operator==(const BigInteger& first, const BigInteger& second);
    friend bool operator!=(const BigInteger& first, const BigInteger& second);
    friend bool operator<(const BigInteger& first, const BigInteger& second);
    friend bool operator>(const BigInteger& first, const BigInteger& second);
    friend bool operator<=(const BigInteger& first, const BigInteger& second);
    friend bool operator>=(const BigInteger& first, const BigInteger& second);
    friend ostream& operator<<(ostream& out, const BigInteger& bi);
    friend istream& operator>>(istream& in, BigInteger& bi);
    friend BigInteger powerOfTen(size_t precision);
    friend class Rational;
private:
    bool sign() const;
    size_t size() const;
    int operator[](size_t index) const;
    void multByInt(int m);
    void normalize();
    void changeSign();
    BigInteger(const vector<int>& n);
    void slice(size_t length, const vector<int>& n);
    void clean();
    static const int BASE = 10000;
    bool neg = false;
    vector<int> number;
};

BigInteger::BigInteger(int _number){
    if(_number == 0){
        number.push_back(0);
        return;
    }
    if(_number < 0){
        neg = true;
        _number *= -1;
    }
    while (_number != 0){
        number.push_back(_number % BASE);
        _number /= BASE;
    }
}

BigInteger::BigInteger(const vector<int>& n){
    number = n;
    normalize();
}

void BigInteger::slice(size_t length, const vector<int>& n){
    if(length > size()){
        return;
    }
    vector<int> newNum(length);
    for(size_t i = 0; i < newNum.size(); ++i){
        newNum[i] = n[n.size() - length + i];
    }
    number = newNum;
}

void BigInteger::changeSign(){
    normalize();
    neg = !neg;
    normalize();
}

void BigInteger::clean(){
    while(number.back() == 0 && number.size() > 1){
        number.pop_back();
    }
    if(number.size() == 1 && number[0] == 0){
        neg = false;
    }
}

int BigInteger::operator[](size_t index) const{
    if(index >= number.size()){
        return 0;
    }
    return number[index];
}

BigInteger BigInteger::operator-() const{
    BigInteger res = *this;
    res.changeSign();
    res.normalize();
    return res;
}

bool BigInteger::sign() const{
    return neg;
}

size_t BigInteger::size() const{
    return number.size();
}

void BigInteger::normalize(){
    if(size() == 0){
            return;
    }
    clean();
    size_t tail = number.size() - 1;
    if(number[tail] < 0){
        neg = !neg;
        for(size_t i = 0; i <= tail; ++i){
            number[i] *= -1;
        }
    }
    for(size_t i = 0; i < tail; ++i){
        while(number[i] >= BASE){
            number[i] -= BASE;
            number[i + 1]++;
        }
        while(number[i] < 0){
            number[i] += BASE;
            number[i + 1]--;
        }
    }
    if(number[tail] >= BASE){
        number.push_back(1);
        number[tail] -= BASE;
        while(number[tail] >= BASE){
            number[tail + 1]++;
            number[tail] -= BASE;
        }
        while(number[tail] < 0){
            --number[tail + 1];
            number[tail] += BASE;
        }
    }
    clean();
}

const string BigInteger::toString(bool nul) const{
    string result;
    if(neg){
        result += "-";
    }
    int start = static_cast<int>(number.size()) - 2;
    if(nul){
        ++start;
    }
    else{
        result += std::to_string(number.back());
    }
    for(int i = start; i >= 0; --i){
        int digNumber = 0;
        int buf = number[i];
        while(buf > 0){
            buf /= 10;
            digNumber++;
        }
        for(int j = 0; j < 4 - digNumber; ++j){
            result += "0";
        }
        if(number[i] != 0){
            result += std::to_string(number[i]);
        }
    }
    return result;
}

BigInteger& BigInteger::operator+=(const BigInteger& second){
    normalize();
    if(second.size() > number.size()){
        size_t dif = second.size() - number.size();
        for(size_t i = 0; i < dif; ++i){
            number.push_back(0);
        }
    }
    if(neg == second.sign()){
        for(size_t i = 0; i < second.size(); ++i){
            number[i] += second[i];
        }
    }
    else{
        for(size_t i = 0; i < second.size(); ++i){
            number[i] -= second[i];
        }
    }
    normalize();
    return *this;
}

const BigInteger operator+(const BigInteger& first, const BigInteger& second){
    BigInteger res = first;
    res += second;
    res.clean();
    return res;
}

const BigInteger operator-(const BigInteger& first, const BigInteger& second){
    BigInteger res = first;
    res -= second;
    res.clean();
    return res;
}

BigInteger& BigInteger::operator-=(const BigInteger& second){
    normalize();
    if(second.size() > number.size()){
        size_t dif = second.size() - number.size();
        for(size_t i = 0; i < dif; ++i){
            number.push_back(0);
        }
    }
    if(neg != second.sign()){
        for(size_t i = 0; i < second.size(); ++i){
            number[i] += second[i];
        }
    }
    else{
        for(size_t i = 0; i < second.size(); ++i){
            number[i] -= second[i];
        }
    }
    normalize();
    return *this;
}

void BigInteger::multByInt(int m){
    if(m >= BASE || m <= -BASE){
        return;
    }
    normalize();
    if(m == 0){
        *this = 0;
        normalize();
        return;
    }
    if(m < 0){
        changeSign();
        m *= -1;
    }
    for(size_t i = 0; i < size(); ++i){
        number[i] *= m;
    }
    normalize();
}

const BigInteger operator*(const BigInteger& first, const BigInteger& second){
    size_t thisSize = max(first.size(), second.size());
    if(thisSize == 1){
        BigInteger res;
        res.number.push_back(first[0] * second[0]);
        res.normalize();
        res.neg = first.sign() ^ second.sign();
        return res;
    }
    thisSize += thisSize & 1;
    BigInteger a;
    BigInteger b;
    BigInteger c;
    BigInteger d;
    for(size_t i = 0; i < thisSize / 2; ++i){
        a.number.push_back(first[thisSize / 2 + i]);
        b.number.push_back(first[i]);
        c.number.push_back(second[thisSize / 2 + i]);
        d.number.push_back(second[i]);
    }
    BigInteger ac = a * c;
    BigInteger bd = b * d;
    BigInteger aPbcPd = (a + b) * (c + d);
    (aPbcPd -= ac) -= bd;
    BigInteger summand1, summand2;
    for(size_t i = 0; i < thisSize; ++i){
        summand1.number.push_back(0);
    }
    for(size_t i = 0; i < thisSize / 2; ++i){
        summand2.number.push_back(0);
    }
    for(size_t i = 0; i < ac.size(); ++i){
        summand1.number.push_back(ac[i]);
    }
    for(size_t i = 0; i < aPbcPd.size(); ++i){
        summand2.number.push_back(aPbcPd[i]);
    }
    return (first.sign() ^ second.sign())?-(summand2 + summand1 + bd):(summand2 + summand1 + bd);
}

BigInteger& BigInteger::operator*=(const BigInteger& second){
    normalize();
    *this = (*this) * second;
    normalize();
    return *this;
}

BigInteger& BigInteger::operator++(){
    normalize();
    if(!neg){
        size_t it = 0;
        while(it < number.size() && number[it] == (BASE - 1)){
            number[it] = 0;
            ++it;
        }
        if(it == number.size()){
            number.push_back(1);
        }
        else{
            ++number[it];
        }
    }
    else{
        size_t it = 0;
        while(it < number.size() && number[it] == 0){
            number[it] = BASE - 1;
            ++it;
        }
        --number[it];
    }
    normalize();
    return *this;
}

const BigInteger BigInteger::operator++(int){
    const BigInteger res = *this;
    ++(*this);
    return res;
}

BigInteger& BigInteger::operator--(){
    neg = !neg;
    ++(*this);
    neg = !neg;
    clean();
    return *this;
}

const BigInteger BigInteger::operator--(int){
    const BigInteger res = *this;
    --(*this);
    return res;
}

bool operator==(const BigInteger& first, const BigInteger& second){
    if(first.sign() != second.sign()){
        return false;
    }
    if(first.size() != second.size()){
        return false;
    }
    for(size_t i = 0; i < first.size(); ++i){
        if(first[i] != second[i]){
            return false;
        }
    }
    return true;
}

bool operator!=(const BigInteger& first, const BigInteger& second){
    return !(first == second);
}

bool operator<(const BigInteger& first, const BigInteger& second){
    if(first.sign() && !(second.sign())){
        return true;
    }
    if(!(first.sign()) && second.sign()){
        return false;
    }
    if(first.size() > second.size()){
        return first.sign();
    }
    if(first.size() < second.size()){
        return !first.sign();
    }
    size_t sz = first.size() - 1;
    for(size_t i = 0; i < first.size(); ++i){
        if(first[sz - i] < second[sz - i]){
            return !first.sign();
        }
        if(first[sz - i] > second[sz - i]){
            return first.sign();
        }
    }
    return false;
}

bool operator>(const BigInteger& first, const BigInteger& second){
    return second < first;
}

bool operator<=(const BigInteger& first, const BigInteger& second){
    return !(first > second);
}

bool operator>=(const BigInteger& first, const BigInteger& second){
    return !(first < second);
}

ostream& operator<<(ostream& out, const BigInteger& bi){
    out << bi.toString();
    return out;
}

istream& operator>>(istream& in, BigInteger& bi){
    string s;
    in >> s;
    bi.number.clear();
    size_t it = s.size() - 1;
    size_t times = (s[0] == '-')?((s.size() - 1) / 4):(s.size() / 4);
    size_t r = (s[0] == '-')?((s.size() - 1) % 4):(s.size() % 4);
    bi.neg = (s[0] == '-');
    for(size_t i = 0; i < times; ++i){
        bi.number.push_back((static_cast<int>(s[it]) - 48) + (static_cast<int>(s[it - 1]) - 48) * 10 +
                            (static_cast<int>(s[it - 2]) - 48) * 100 + (static_cast<int>(s[it - 3]) - 48) * 1000);
        it -= 4;
    }
    int deg = 1;
    int last = 0;
    for(size_t i = 0; i < r; ++i){
        last += ((static_cast<int>(s[it]) - 48) * deg);
        deg *= 10;
        --it;
    }
    bi.number.push_back(last);
    bi.normalize();
    return in;
}

BigInteger::operator bool() const{
    return !((number.size() == 1) && (number[0] == 0));
}

BigInteger& BigInteger::operator/=(const BigInteger& second){
    if(second == 0){
        return *this;
    }
    normalize();
    bool sgn = neg ^ second.sign();
    if(neg){
        changeSign();
    }
    BigInteger sec = second;
    if(sec.sign()){
        sec.changeSign();
    }
    if(*this < sec){
        *this = 0;
        return *this;
    }
    vector<int> result;
    size_t initSize = size();
    for(size_t i = sec.size(); i <= initSize; ++i){
        int l = 0;
        int r = BASE - 1;
        bool found = false;
        BigInteger cut = *this;
        cut.slice(i, number);
        cut.normalize();
        BigInteger buf = sec;
        while(l != r){
            buf = sec;
            buf.multByInt((l + r) / 2);
            if(buf > cut){
                r = (l + r) / 2;
            }
            else if(buf == cut){
                result.push_back((l + r) / 2);
                found = true;
                vector<int> d(initSize - i, 0);
                for(size_t i = 0; i < buf.size(); ++i){
                    d.push_back(buf[i]);
                }
                BigInteger bi(d);
                *this -= bi;
                break;
            }
            else{
                l = (l + r + 1) / 2;
            }
        }
        if(!found){
            if((sec * r) <= cut){
                result.push_back(r);
            }
            else{
                result.push_back(r - 1);
            }
            vector<int> d(initSize - i, 0);
            BigInteger x = sec;
            x.multByInt(result.back());
            for(size_t i = 0; i < x.size(); ++i){
                d.push_back(x[i]);
            }
            BigInteger bi(d);
            *this -= bi;
        }
        while(size() < initSize){
            number.push_back(0);
        }
    }
    for(size_t i = 0; i < result.size() / 2; ++i){
        swap(result[i], result[result.size() - 1 - i]);
    }
    number = result;
    neg = sgn;
    normalize();
    return *this;
}

const BigInteger operator/(const BigInteger& first, const BigInteger& second){
    BigInteger res = first;
    res /= second;
    return res;
}

BigInteger& BigInteger::operator%=(const BigInteger& second){
    if(second == 2){
        *this = (number[0] % 2) * (neg?(-1):1);
        return *this;
    }
    BigInteger div = *this / second;
    *this -= (div * second);
    return *this;
}

const BigInteger operator%(const BigInteger& first, const BigInteger& second){
    if(second == 2){
        return (first.number[0] & 1) * (first.sign()?-1:1);
    }
    BigInteger res = first;
    res %= second;
    return res;
}

class Rational{
public:
    Rational(const BigInteger& bi): num(bi){}
    Rational(int i): num(i){}
    Rational() = default;
    const Rational operator-() const;
    Rational& operator+=(const Rational& second);
    friend const Rational operator+(const Rational& first, const Rational& second);
    Rational& operator-=(const Rational& second);
    friend const Rational operator-(const Rational& first, const Rational& second);
    Rational& operator*=(const Rational& second);
    friend const Rational operator*(const Rational& first, const Rational& second);
    Rational& operator/=(const Rational& second);
    friend const Rational operator/(const Rational& first, const Rational& second);
    friend bool operator==(const Rational& first, const Rational& second);
    friend bool operator!=(const Rational& first, const Rational& second);
    friend bool operator<(const Rational& first, const Rational& second);
    friend bool operator>(const Rational& first, const Rational& second);
    friend bool operator<=(const Rational& first, const Rational& second);
    friend bool operator>=(const Rational& first, const Rational& second);
    const string toString() const;
    const string asDecimal(size_t precision = 0) const;
    explicit operator double() const;
private:
    const BigInteger& numer() const;
    const BigInteger& denom() const;
    void changeSign();
    bool sign() const;
    void normalizeS();
    const BigInteger GCD(const BigInteger& first, const BigInteger& second) const;
    void normalize();
    BigInteger num = 0;
    BigInteger den = 1;
};

bool Rational::sign() const{
    return num.sign();
}

const BigInteger& Rational::numer() const{
    return num;
}

const BigInteger& Rational::denom() const{
    return den;
}

void Rational::changeSign(){
    if(num != 0){
        num.changeSign();
    }
}

const Rational Rational::operator-() const{
    Rational res = *this;
    res.changeSign();
    return res;
}

const BigInteger Rational::GCD(const BigInteger& first, const BigInteger& second) const{
    BigInteger a = first;
    if(a.sign()){
        a.changeSign();
    }
    BigInteger b = second;
    if(b.sign()){
        b.changeSign();
    }
    while(a != 0 && b != 0){
        if(a > b){
            a %= b;
        }
        else{
            b %= a;
        }
    }
    if(a != 0){
        return a;
    }
    else{
        return b;
    }
}

void Rational::normalize(){
    normalizeS();
    BigInteger g = GCD(num, den);
    num /= g;
    den /= g;
    normalizeS();
}

Rational& Rational::operator+=(const Rational& second){
    normalizeS();
    num *= second.denom();
    num += (den * second.numer());
    den *= second.denom();
    normalize();
    return *this;
}

const Rational operator+(const Rational& first, const Rational& second){
    Rational res = first;
    res += second;
    return res;
}

Rational& Rational::operator-=(const Rational& second){
    normalizeS();
    changeSign();
    *this += second;
    changeSign();
    normalizeS();
    return *this;
}

const Rational operator-(const Rational& first, const Rational& second){
    Rational res = first;
    res -= second;
    return res;
}

Rational& Rational::operator*=(const Rational& second){
    normalizeS();
    if(num == 0 || second.numer() == 0){
        num = 0;
        den = 1;
        return *this;
    }
    BigInteger d1 = GCD(den, second.numer());
    BigInteger d2 = GCD(num, second.denom());
    num /= d2;
    den /= d1;
    num *= (second.numer() / d1);
    den *= (second.denom() / d2);
    normalizeS();
    return *this;
}

const Rational operator*(const Rational& first, const Rational& second){
    Rational res = first;
    res *= second;
    return res;
}

Rational& Rational::operator/=(const Rational& second){
    normalizeS();
    if(num == 0 || second.numer() == 0){
        num = 0;
        den = 1;
        return *this;
    }
    BigInteger d1 = GCD(den, second.denom());
    BigInteger d2 = GCD(num, second.numer());
    num /= d2;
    den /= d1;
    num *= (second.denom()/ d1);
    den *= (second.numer() / d2);
    normalizeS();
    return *this;
}

const Rational operator/(const Rational& first, const Rational& second){
    Rational res = first;
    res /= second;
    return res;
}

bool operator==(const Rational& first, const Rational& second){
    return(first.numer() == second.numer() && first.denom() == second.denom());
}

bool operator!=(const Rational& first, const Rational& second){
    return !(first == second);
}

bool operator<(const Rational& first, const Rational& second){
    if(first.sign() && !second.sign()){
        return true;
    }
    if(!first.sign() && second.sign()){
        return false;
    }
    return (first.numer() * second.denom()) < (second.numer() * first.denom());
}

bool operator>(const Rational& first, const Rational& second){
    return second < first;
}

bool operator<=(const Rational& first, const Rational& second){
    return !(first > second);
}

bool operator>=(const Rational& first, const Rational& second){
    return !(first < second);
}

const string Rational::toString() const{
    string res = num.toString();
    if(den > 1){
        (res += "/") += den.toString();
    }
    return res;
}

static BigInteger powerOfTen(size_t precision){
    BigInteger p;
    size_t amount = precision / 4;
    size_t r = precision % 4;
    for(size_t i = 0; i < amount; ++i){
        p.number.push_back(0);
    }
    int last = 1;
    for(size_t i = 0; i < r; ++i){
        last *= 10;
    }
    p.number.push_back(last);
    return p;
}

const string Rational::asDecimal(size_t precision) const{
    if(precision == 0){
        return (num / den).toString();
    }
    if(num == 0){
        string res = "0.";
        res.insert(2, precision, '0');
        return res;
    }
    BigInteger cpNum = num;
    bool sgn = cpNum.sign();
    if(cpNum.sign()){
        cpNum.changeSign();
    }
    BigInteger div = cpNum / den;
    BigInteger r = cpNum - div * den;
    size_t position = (cpNum / den).toString().size();
    string res;
    BigInteger x = (cpNum * powerOfTen(precision)) / den;
    string integ = ((cpNum * powerOfTen(precision)) / den).toString();
    if(div == 0){
        res = "0";
        if(static_cast<int>(precision) - static_cast<int>(integ.size()) > 0){
            res.insert(1, precision - integ.size(), '0');
        }
        res += integ;
        res.insert(position, 1, '.');
    }
    else{
        res = integ;
        res.insert(position, 1, '.');
    }
    if(sgn && x != 0){
        res.insert(0, 1, '-');
    }
    return res;
}

void Rational::normalizeS(){
    if(num == 0){
        den = 1;
        return;
    }
    if(den.sign()){
        den.changeSign();
        if(num != 0){
            num.changeSign();
        }
    }
    return;
}

Rational::operator double() const{
    BigInteger div = num / den;
    BigInteger r = (num - den * div);
    r *= powerOfTen(20);
    r /= den;
    double res = 0;
    double buf = 1;
    for(size_t i = 0; i < div.size(); ++i){
        res += (buf * static_cast<double>(div[i]));
        buf *= 10000.0;
    }
    buf = 0.0001;
    for(size_t i = 0; i < r.size() && i < 5; ++i){
        res += r[r.size() - i - 1] * buf;
        buf *= 0.0001;
    }
    return res;
}
