#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <assert.h>
#include <queue>

using namespace std;

struct Node{
    int key;
    Node* lchild = nullptr;
    Node* rchild = nullptr;

    Node(int _key) : key(_key){}

    ~Node(){
        delete lchild;
        delete rchild;
    }
};

class BST{
    public:
        void insert(int _key);
        BST(){
            root = nullptr;
        }
        ~BST(){
            delete root;
        }
        void print();
    private:
        Node* root = nullptr;
};

void BST::insert(int _key){
    Node* newNode = new Node(_key);
    if(root == nullptr){
        root = newNode;
        return;
    }
    Node* pointer = root;
    while(true){
        if(_key < pointer->key){
            if(pointer->lchild == nullptr){
                pointer->lchild = newNode;
                break;
            }
            else{
                pointer = pointer->lchild;
            }
        }
        else{
            if(pointer->rchild == nullptr){
                pointer->rchild = newNode;
                break;
            }
            else{
                pointer = pointer->rchild;
            }
        }
    }

}

void BST::print(){
    queue<Node*> q;
    q.push(root);
    Node* it;
    while(q.size()){
        it = q.front();
        if(it->lchild != nullptr){
            q.push(it->lchild);
        }
        if(it->rchild != nullptr){
            q.push(it->rchild);
        }
        cout << it->key << ' ';
        q.pop();
        }
    it = nullptr;
}

int main(){
    BST tree;
    int n;
    int a = 0;
    cin >> n;
    for(int i = 0; i < n; ++i){
        cin >> a;
        tree.insert(a);
    }
    tree.print();
    return 0;
}
