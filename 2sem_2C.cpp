/*Сейчас Рику надо попасть из вселенной с номером S во вселенную с номером F.
Он знает все существующие телепорты, и казалось бы нет никакой проблемы.
Но, далеко не секрет, что за свою долгую жизнь Рик поссорился много с кем.
Из своего личного опыта он знает, что при телепортациях есть вероятность,
что его заставят ответить за свои слова.

Если Рик знает вероятности быть прижатым к стенке на всех существующих телепортациях,
помогите ему посчитать минимальную вероятность,
что он всё-таки столкнется с неприятностями.*/
#include <iostream>
#include <vector>

using namespace std;

struct Edge{
    Edge(size_t _second, double _cost): second(_second), cost(_cost){}
    size_t second;
    double cost;
};

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    void AddEdge(size_t first, size_t second, double cost);
    size_t size() const;
    void GetNeighbours(size_t vertex, vector<Edge>& neighbours) const;
private:
    vector<vector<Edge>> edges;
};

void Graph::AddEdge(size_t first, size_t second, double cost){
    edges[first].emplace_back(second, cost);
    edges[second].emplace_back(first, cost);
}

size_t Graph::size() const{
    return edges.size();
}

void Graph::GetNeighbours(size_t vertex, vector<Edge>& neighbours) const{
    neighbours = edges[vertex];
}

double FindMostSafePath(const Graph& g, size_t start, size_t fin){
    vector<vector<double>> prob(g.size(), vector<double>(g.size(), 2.0));
    for(size_t i = 0; i < g.size(); ++i){
        prob[start][i] = 0.0;
    }
    for(size_t i = 1; i < g.size(); ++i){
        for(size_t j = 0; j < g.size(); ++j){
            vector<Edge> neighbours;
            g.GetNeighbours(j, neighbours);
            for(size_t t = 0; t < neighbours.size(); ++t){
                prob[neighbours[t].second][i] = min(min(prob[neighbours[t].second][i - 1], prob[neighbours[t].second][i]),
                                                     prob[j][i - 1] + (1 - prob[j][i - 1]) * neighbours[t].cost);
            }
        }
    }
    return prob[fin][g.size() - 1];
}

int main(){
    size_t n, m, s, f;
    cin >> n >> m >> s >> f;
    Graph g(n);
    for(size_t i = 0; i < m; ++i){
        size_t si, fi;
        double pi;
        cin >> si >> fi >> pi;
        g.AddEdge(si - 1, fi - 1, pi / 100.0);
    }
    cout << FindMostSafePath(g, s - 1, f - 1);
    return 0;
}
