/*���� ����� N � ������������������ �� N ����� �����. ����� ������ ���������� ���������� �� �������� ����������.
��� ������� ������ ����������� ��������� ������ Sparse Table.
��������� ����� ��������� ������� ��������� O(1). ����� ���������� ��������� ������ O(n log n).*/
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

class SparseTable{
public:
    SparseTable(const vector<int>& arr);
    size_t PosOfRangeMinimumQuery(size_t left, size_t right) const;
    int operator[](size_t pos) const{return data[pos];}
private:
    vector<int> data;
    vector<vector<int>> table;
};

SparseTable::SparseTable(const vector<int>& arr): data(arr), table(static_cast<size_t>(log2(data.size())) + 1){
    for(size_t i = 0; i < data.size(); ++i){
        table[0].push_back(i);
    }
    for(size_t k = 1; k < table.size(); ++k){
        for(size_t i = 0; i < data.size() - (1 << k) + 1; ++i){
            size_t left = table[k - 1][i];
            size_t right = table[k - 1][i + (1 << (k - 1))];
            table[k].push_back((data[left] < data[right])?left:right);
        }
    }
}

size_t SparseTable::PosOfRangeMinimumQuery(size_t left, size_t right) const{
    size_t maxLength = static_cast<size_t>(log2(right - left + 1));
    size_t leftMin = table[maxLength][left];
    size_t rightMin = table[maxLength][right - (1 << maxLength) + 1];
    return ((data[leftMin] < data[rightMin])?leftMin:rightMin);
}

int SecondOrderStatistics(const SparseTable& data, size_t left, size_t right){
    size_t posOfMin = data.PosOfRangeMinimumQuery(left, right);
    if(posOfMin == left){
        return data[data.PosOfRangeMinimumQuery(left + 1, right)];
    }
    if(posOfMin == right){
        return data[data.PosOfRangeMinimumQuery(left, right - 1)];
    }
    return min(data[data.PosOfRangeMinimumQuery(left, posOfMin - 1)], data[data.PosOfRangeMinimumQuery(posOfMin + 1, right)]);
}

int main(){
    size_t n, m;
    cin >> n >> m;
    vector<int> arr(n);
    for(size_t i = 0; i < arr.size(); ++i){
        cin >> arr[i];
    }
    SparseTable table(arr);
    for(size_t i = 0; i < m; ++i){
        size_t a, b;
        cin >> a >> b;
        cout << SecondOrderStatistics(table, a - 1, b - 1) << endl;
    }
    return 0;
}
