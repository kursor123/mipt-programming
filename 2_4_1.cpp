#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <stdio.h>
#include <assert.h>
#include <new>

using namespace std;

void Merge(vector<int>& arr, int left, int right, vector<int>& result){
    int it1 = left;
    int border = (left + right) / 2;
    int it2 = border + 1;

    int resultSize = right - left + 1;

    for(int i = 0; i < resultSize; ++i){
        if(it1 == border + 1){
            result[i] = arr[it2];
            it2++;
        }
        else{
            if(it2 == right + 1){
                result[i] = arr[it1];
                it1++;
            }
            else{
                if(arr[it1] <= arr[it2]){
                    result[i] = arr[it1];
                    it1++;
                }
                else{
                    result[i] = arr[it2];
                    it2++;
                }
            }
        }
    }
}

void MergeSort(vector<int>& arr, int left, int right){
    if(left == right){
        return;
    }
    else{
        int arrSize = right - left + 1;
        vector<int> additiveArray(arrSize);

        MergeSort(arr, left, (left + right) / 2);
        MergeSort(arr, (left + right) / 2 + 1, right);
        Merge(arr, left, right, additiveArray);
        for(int i = left; i <= right; ++i){
            arr[i] = additiveArray[i - left];
        }
    }
}

int main(){
    int n = 0;
    int k = 0;

    cin >> n >> k;

    if(n < 2 * k){
        vector<int> first2K;
        int element;
        for(int i = 0; i < n; ++i){
            cin >> element;
            first2K.push_back(element);
        }
        MergeSort(first2K, 0, n - 1);
        for(int i = 0; i < n; ++i){
            cout << first2K[i] << ' ';
        }
    }
    else{
        vector<int> first2K(2 * k);
        vector<int> additiveArray(2 * k);

        for(int i = 0; i < 2 * k; ++i){
            cin >> first2K[i];
        }

        MergeSort(first2K, 0, 2 * k - 1);

        for(int j = 0; j < n / k - 2; ++j){
            for(int i = k; i < 2 * k; ++i){
                cin >> first2K[i];
            }

            MergeSort(first2K, k, 2 * k - 1);
            Merge(first2K, 0, 2 * k - 1, additiveArray);

            for(int i = 0; i < k; ++i){
                first2K[i] = additiveArray[i];
            }
        }

        if(n % k == 0){
            for(int i = 0; i < k; ++i){
                cout << first2K[i] << ' ';
            }
        }
        else{
            for(int i = k; i < k + n % k; ++i){
                cin >> first2K[i];
            }
            MergeSort(first2K, 0, k + n % k - 1);
            for(int i = 0; i < k; ++i){
                cout << first2K[i] << ' ';
            }
        }
    }
    return 0;

}
