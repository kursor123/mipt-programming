#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;



class HashTable{
public:
    bool insert(const string& str);
    bool find(const string& str, int& _adress) const;
    bool remove(const string& str);
    void state() const;
    HashTable() : data(capacity) {}
    HashTable(int initCapacity) : capacity(initCapacity), size(0), data(initCapacity) {}

private:
    int capacity = 8;
    int size = 0;
    struct Node{
        string key;
        bool isEmpty = true;
        bool isDeleted = false;
        Node(const string& _key) : key(_key), isEmpty(false), isDeleted(false){};
        Node() = default;
    };
    vector<Node> data;
    int HashFunction1(const string& str) const;
    int HashFunction2(const string& str) const;
    void ReHash(int _capacity);
    bool find(const string& str, int place, int& adress) const;
};

void HashTable::state() const{
    cout << "capacity: " << capacity << '\n';
    cout << "size: " << size << '\n';
    for(int i = 0; i < data.size(); ++i){
        if(!data[i].isEmpty){
            cout << data[i].key << ' ';
        }
    }
}

int HashTable::HashFunction1(const string& str) const{
    int hash = 0;
    for(int i = 0; i < str.size(); ++i){
        hash = (hash * 7 + str[i]) % capacity;
    }
    return hash;
}

int HashTable::HashFunction2(const string& str) const{
    int hash = 0;
    for(int i = str.size() - 1; i >= 0; --i){
        hash = ((hash * 13 + str[i])) % capacity;
    }
    hash = (2 * hash + 1) % capacity;
    return hash;
}

bool HashTable::insert(const string& str){
    int hash1 = HashFunction1(str);
    int hash2 = HashFunction2(str);
    Node _node(str);
    int it = hash1;
    while(!data[it].isEmpty){
        if(data[it].key == str){
            return false;
        }
        it = (it + hash2) % capacity;
        if(it == hash1){
            return false;
        }
    }
    int a = 0;
    if(data[it].isDeleted && find(str, it, a)){
        return false;
    }
    data[it] = _node;
    size++;
    if(((double)size / (double)capacity) > 0.75){
        ReHash(2 * capacity);
    }
    return true;
}

bool HashTable::find(const string& str, int place, int& _adress) const{
    int hash1 = HashFunction1(str);
    int hash2 = HashFunction2(str);
    int it = place;
    while((!data[it].isEmpty) || data[it].isDeleted){
        if(data[it].key == str){
            _adress = it;
            return true;
        }
        it = (it + hash2) % capacity;
        if(it == hash1){
            break;
        }
    }
    return false;
}

bool HashTable::find(const string& str, int& _adress) const{
    int hash1 = HashFunction1(str);
    int hash2 = HashFunction2(str);
    int it = hash1;
    while((!data[it].isEmpty) || data[it].isDeleted){
        if(data[it].key == str){
            _adress = it;
            return true;
        }
        it = (it + hash2) % capacity;
        if(it == hash1){
            break;
        }
    }
    return false;
}

bool HashTable::remove(const string& str){
    int adress = 0;
    if(!find(str, adress)){
        return false;
    }
    data[adress].key.clear();
    data[adress].isEmpty = true;
    data[adress].isDeleted = true;
    size--;
    return true;
}

void HashTable::ReHash(int _capacity){
    HashTable newTable(_capacity);
    for(int i = 0; i < data.size(); ++i){
        if(!data[i].isEmpty){
            newTable.insert(data[i].key);
        }
    }
    capacity = _capacity;
    data = newTable.data;
}

int main(){
    HashTable h(2048);
    char command;
    string s;
    int a = 0;
    while(cin >> command && cin >> s){
        if(command == '+'){
            if(h.insert(s)){
                cout << "OK" << endl;
            }
            else{
                cout << "FAIL" << endl;
            }
        }
        if(command == '?'){
            if(h.find(s, a)){
                cout << "OK" << endl;
            }
            else{
                cout << "FAIL" << endl;
            }
        }
        if(command == '-'){
            if(h.remove(s)){
                cout << "OK" << endl;
            }
            else{
                cout << "FAIL" << endl;
            }
        }
    }
    return 0;
}
