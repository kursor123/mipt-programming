/*ways[n][m] - это число способов представить число n в виде суммы слагаемых, каждое из которых не превосходит m.
ways[i][0] (i != 0) = 0, т.к. нельзя представить натуральное число суммой, где каждое слагаемое неположительное.
ways[0][i] = 1, т.к. единственный способ представить 0 как сумму положительных слагаемых - не взять ни одно из них.
Это фиктивный ход, который поможет нам в динамике. Далее, считаем ways[i][j].
В разбиении мы либо берем число j, либо нет. Если мы не берем его, то число таких разбиений есть ways[i][j - 1].
Если мы берем j, то это равно числу разбиений i - j на слагемые, не превосходящие j опять-таки, то есть ways[i - j][j].
В случае, если j > i, мы взять j не можем и сразу переходим к ways[i][j - 1].*/
#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

const int MAXN = 201;

void CountWays(vector<vector<unsigned long long> >& ways, int N){
    for(int i = 0; i < N + 1; ++i){
        ways[i][0] = 0;
        ways[0][i] = 1;
    }
    for(int i = 1; i < N + 1; ++i){
        for(int j = 1; j < N + 1; ++j){
            if(j > i){
                ways[i][j] = ways[i][j - 1];
            }
            else{
                ways[i][j] = ways[i][j - 1] + ways[i - j][j];
            }
        }
    }
}

int main(){
    int N;
    cin >> N;
    vector<vector<unsigned long long> > ways;
    for(int i = 0; i < MAXN; ++i){
        ways.emplace_back(MAXN);
    }
    CountWays(ways, N);
    cout << ways[N][N];
    return 0;
}
