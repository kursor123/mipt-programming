/*����� ���������� ���������� � �����, ����� ������ ���������� ��������� �����.
� ������ ��� ���� ����� ����� ������ ����������������� ����.
��� ����� ������ ������ � ����� ����� � ������� ���� ��� ��� � ������������ ���� ������,
���� ���� �� ����� ��������� �������. ���� �� ����� ���� ��� ����� � ����� ��������������� ���.
���� ������������ ����� ���� �� n x m ������, ������ ��������� ������ ��� �������� ������.
� ����� ���� ������ ������� ������� 1 x 2, ��������� ������� �������� � ���� a ������, � 1 x 1,
��������� ������� �������� b ������. ��� ���������� ������� ��� ������ ������, ������ ������ ��, �� ���������� ������� ���� �� �����.
����������, ����� ����� ����������� ���������� ������ ���� � ��� ������ ���������� ���� ���� ������.*/
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    ~Graph() = default;
    void AddEdge(size_t first, size_t second);
    size_t Size() const;
    vector<size_t> GetNeighbours(size_t vertex) const;
private:
    vector<vector<size_t>> edges;
};

void Graph::AddEdge(size_t first, size_t second){
    edges[first].push_back(second);
}

size_t Graph::Size() const{
    return edges.size();
}

vector<size_t> Graph::GetNeighbours(size_t vertex) const{
    return edges[vertex];
}

Graph BuildGraph(const vector<string>& bridges){
    vector<vector<size_t>> toNum(bridges.size(), vector<size_t>(bridges[0].size()));
    vector<pair<size_t, size_t>> toPair(bridges.size() * bridges[0].size());
    size_t cnt = 0;
    for(size_t i = 0; i < bridges.size(); ++i){
        for(size_t j = 0; j < bridges[i].size(); ++j){
            if(bridges[i][j] == '*'){
                toNum[i][j] = cnt;
                toPair[cnt] = make_pair(i, j);
                cnt++;
            }
        }
    }
    Graph g(cnt);
    for(size_t i = 0; i < cnt; ++i){
        pair<size_t, size_t> p = toPair[i];
        if(p.second != 0 && bridges[p.first][p.second - 1] == '*'){
            g.AddEdge(i, toNum[p.first][p.second - 1]);
        }
        if(p.second != bridges[0].size() - 1 && bridges[p.first][p.second + 1] == '*'){
            g.AddEdge(i, toNum[p.first][p.second + 1]);
        }
        if(p.first != 0 && bridges[p.first - 1][p.second] == '*'){
            g.AddEdge(i, toNum[p.first - 1][p.second]);
        }
        if(p.first != bridges.size() - 1 && bridges[p.first + 1][p.second] == '*'){
            g.AddEdge(i, toNum[p.first + 1][p.second]);
        }
    }
    return g;
}

void ColorVertices(const Graph& g, vector<bool>& used, vector<bool>& colors, bool color, size_t vertex){
    used[vertex] = true;
    colors[vertex] = color;
    for(auto nb : g.GetNeighbours(vertex)){
        if(used[nb]){
            continue;
        }
        ColorVertices(g, used, colors, !color, nb);
    }
}

vector<size_t> DivideByTwoParts(const Graph& g){
    vector<bool> used(g.Size(), false);
    vector<bool> colors(g.Size(), true);
    for(size_t i = 0; i < g.Size(); ++i){
        if(used[i]){
            continue;
        }
        ColorVertices(g, used, colors, false, i);
    }
    vector<size_t> odd;
    vector<size_t> even;
    for(size_t i = 0; i < colors.size(); ++i){
        if(colors[i]){
            odd.push_back(i);
        }
        else{
            even.push_back(i);
        }
    }
    return ((odd.size() < even.size())?odd:even);
}

bool FindIncreasingPath(const Graph& g, vector<bool>& used, vector<int>& matching,  size_t vertex){
    //�������� ����
    if(used[vertex]){
        return false;
    }
    used[vertex] = true;
    for(size_t e : g.GetNeighbours(vertex)){
        if(matching[e] == -1 || FindIncreasingPath(g, used, matching, matching[e])){
            matching[e] = vertex;
            return true;
        }
    }
    return false;
}

int FindMinCost(const Graph& g, int dominoCost, int cellCost){
    if(2 * cellCost <= dominoCost){
        return g.Size() * cellCost;
    }
    vector<bool> used(g.Size(), false);
    vector<int> matching(g.Size(), -1);
    vector<size_t> part = DivideByTwoParts(g);
    for(size_t i = 0; i < part.size(); ++i){
        if(FindIncreasingPath(g, used, matching, part[i])){
            used.assign(g.Size(), false);
        }
    }
    size_t cnt = 0;
    for(size_t i = 0; i < matching.size(); ++i){
        if(matching[i] != -1){
            ++cnt;
        }
    }
    return cnt * dominoCost + (g.Size() - 2 * cnt) * cellCost;
}

int main(){
    size_t n, m;
    int a, b;
    cin >> n >> m >> a >> b;
    vector<string> bridge;
    for(size_t i = 0; i < n; ++i){
        string s;
        cin >> s;
        bridge.push_back(s);
    }
    Graph g = BuildGraph(bridge);
    cout << FindMinCost(g, a, b);
    return 0;
}
