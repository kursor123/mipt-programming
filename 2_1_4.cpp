#include <iostream>
#include <cmath>
#include <string>
#include <vector>

using namespace std;

bool IsMore(string a, string b){
    int i = 0;
    int border = max(a.size(), b.size());
    while(i < border){
        if (a[i] > b[i]){
            return true;
        }
        else{
            if(a[i] < b[i]){
                return false;
            }
            else{
                ++i;
            }
        }
    }
    return false;
}

void InsertionSort(vector<string>& arr){
    int i = 0;
    int j = 0;
    int sizeOfArr = arr.size();
    for(i = 1; i < sizeOfArr; ++i){
        j = i;
        while(IsMore(arr[j - 1], arr[j])){
            swap(arr[j - 1], arr[j]);
            j--;
            if(j == 0){
                break;
            }
        }
    }
}

int main(){
    vector <string> arr;

    int n = 0;
    cin >> n;

    string str;
    for(int i = 0; i < n; ++i){
        cin >> str;
        arr.push_back(str);
    }


    InsertionSort(arr);
        for(int i = 0; i < n; ++i){
        cout << arr[i] << '\n';
    }
    return 0;
}
