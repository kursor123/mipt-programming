/*Задано дерево с корнем, содержащее (1 ≤ n ≤ 100 000) вершин, пронумерованных от 0 до n-1.
Требуется ответить на m (1 ≤ m ≤ 10 000 000) запросов о наименьшем общем предке для пары вершин.
Запросы генерируются следующим образом. Заданы числа a1, a2 и числа x, y и z.
Числа a3, ..., a2m генерируются следующим образом: ai = (x * ai-2 + y * ai-1 + z) mod n.
Первый запрос имеет вид (a1, a2). Если ответ на i-1-й запрос равен v, то i-й запрос имеет вид ((a2i-1 + v) mod n, a2i).
Для решения задачи можно использовать метод двоичного подъёма.*/
#include <iostream>
#include <vector>
#include <cmath>
#include <memory>

using namespace std;

struct Node{
    explicit Node(int _data): data(_data){}
    int data;
    shared_ptr<Node> parent = nullptr;
    shared_ptr<Node> next = nullptr;
    shared_ptr<Node> first = nullptr;
};

class LCAFinder{
public:
    LCAFinder(const vector<int>& edges);
    int LCA(int firstVertex, int secondVertex) const;
private:
    void DFS(shared_ptr<Node> vertex, vector<int>& time, int& curTime);
    void BuildTree(const vector<int>& edges);
    bool IsParent(int parent, int child) const;
    shared_ptr<Node> root = nullptr;
    vector<vector<int>> parents;
    vector<int> in;
    vector<int> out;
};

void LCAFinder::DFS(shared_ptr<Node> vertex, vector<int>& time, int& curTime){
    curTime++;
    time.push_back(vertex->data);
    in[vertex->data] = curTime;
    if(vertex->first != nullptr){
        parents[vertex->first->data][0] = vertex->data;
        DFS(vertex->first, time, curTime);
    }
    curTime++;
    out[vertex->data] = curTime;
    time.push_back(vertex->data);
    if(vertex->next != nullptr){
        parents[vertex->next->data][0] = parents[vertex->data][0];
        DFS(vertex->next, time, curTime);
    }
}

void LCAFinder::BuildTree(const vector<int>& edges){
    vector<shared_ptr<Node>> nodes(edges.size() + 1);
    for(size_t i = 0; i < nodes.size(); ++i){
        nodes[i] = make_shared<Node>(i);
    }
    root = nodes[0];
    vector<int> last(nodes.size(), -1);
    for(size_t i = 0; i < edges.size(); ++i){
        int parent = edges[i];
        nodes[i + 1]->parent = nodes[parent];
        if(last[parent] == -1){
            nodes[parent]->first = nodes[i + 1];
        }
        else{
            nodes[last[parent]]->next = nodes[i + 1];
        }
        last[parent] = i + 1;
    }
    in = vector<int>(nodes.size());
    out = vector<int>(nodes.size());
}

LCAFinder::LCAFinder(const vector<int>& edges){
    BuildTree(edges);
    vector<int> time;
    int curTime = 0;
    for(size_t i = 0; i < in.size(); ++i){
        parents.push_back(vector<int>(static_cast<int>(log2(in.size())) + 1));
    }
    parents[0][0] = 0;
    DFS(root, time, curTime);
    for(size_t i = 1; i < static_cast<size_t>(log2(in.size())) + 1; ++i){
        for(size_t v = 0; v < in.size(); ++v){
            parents[v][i] = parents[parents[v][i - 1]][i - 1];
        }
    }
}

bool LCAFinder::IsParent(int parent, int child) const{
    return in[parent] < in[child] && out[parent] > out[child];
}

int LCAFinder::LCA(int firstVertex, int secondVertex) const{
    if(firstVertex == secondVertex){
        return firstVertex;
    }
    if(IsParent(firstVertex, secondVertex)){
        return firstVertex;
    }
    if(IsParent(secondVertex, firstVertex)){
        return secondVertex;
    }
    int height = parents[0].size() - 1;
    while(height >= 0){
        if(!IsParent(parents[firstVertex][height], secondVertex)){
            firstVertex = parents[firstVertex][height];
        }
        height--;
    }
    return parents[firstVertex][0];
}

int main(){
    int n, m;
    cin >> n >> m;
    vector<int> edges(n - 1);
    for(size_t i = 0; i < edges.size(); ++i){
        cin >> edges[i];
    }
    LCAFinder t(edges);
    unsigned long long sum = 0;
    long long a1, a2;
    cin >> a1 >> a2;
    long long x, y, z;
    cin >> x >> y >> z;
    int v = 0;
    for(size_t i = 0; i < m; ++i){
        v = t.LCA((a1 + v) % n, a2);
        sum += v;
        long long a3 = (x * a1 + y * a2 + z) % n;
        long long a4 = (x * a2 + y * a3 + z) % n;
        a1 = a3;
        a2 = a4;
    }
    cout << sum;
    return 0;
}
