/*Требуется найти в связном графе остовное дерево минимального веса.
Воспользуйтесь алгоритмом Крускала.*/
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Edge{
public:
    Edge(int _first, int _second, int _cost): first(_first), second(_second), cost(_cost){}
    friend bool operator<(const Edge& e1, const Edge& e2){return e1.cost < e2.cost;}
    friend bool operator==(const Edge& e1, const Edge& e2){return !(e1 < e2 || e2 < e1);}
    int GetFirst() const{return first;}
    int GetSecond() const{return second;}
    int GetCost() const{return cost;}
private:
    int first;
    int second;
    int cost;
};

class DSU{
public:
    DSU(size_t n): size(n, 1){
        for(size_t i = 0; i < n; ++i){
            parent.push_back(i);
        }
    }
    int GetParent(int v);
    void Unite(int v1, int v2);
private:
    vector<int> parent;
    vector<int> size;
};

int DSU::GetParent(int v){
    if(parent[v] == v){
        return v;
    }
    int x = GetParent(parent[v]);
    parent[v] = x;
    return x;
}

void DSU::Unite(int p1, int p2){
    if(p1 == p2){
        return;
    }
    if(size[p1] > size[p2]){
        swap(p1, p2);
    }
    parent[p1] = p2;
    size[p2] += size[p1];
}

int FindWeightOfMST(vector<Edge> edges, int sz){
    sort(edges.begin(), edges.end());
    DSU forest(sz);
    int sum = 0;
    int cnt = 0;
    for(size_t i = 0; i < edges.size(); ++i){
        int f = edges[i].GetFirst(), s = edges[i].GetSecond();
        int p1 = forest.GetParent(f), p2 = forest.GetParent(s);
        if(p1 == p2){
            continue;
        }
        ++cnt;
        sum += edges[i].GetCost();
        if(cnt == sz - 1){
            break;
        }
        forest.Unite(p1, p2);
    }
    return sum;
}

int main(){
    size_t n, m;
    cin >> n >> m;
    vector<Edge> edges;
    for(size_t i = 0; i < m; ++i){
        int f, s, c;
        cin >> f >> s >> c;
        edges.emplace_back(f - 1, s - 1, c);
    }
    cout << FindWeightOfMST(edges, n);
    return 0;
}
