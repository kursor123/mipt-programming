#include <iostream>
#include <cmath>
#include <string>
#include <vector>
#include <cstdlib>
#include <assert.h>
#include <stdio.h>

using namespace std;

int FindPivotIndex(vector<int>& arr, int left, int right){
    int middle = (left + right) / 2;
    int value = max(max(min(arr[left], arr[right]), min(arr[left], arr[middle])), min(arr[middle], arr[right]));
    if(arr[left] == value){
        return left;
    }
    else{
        if(arr[middle] == value){
            return middle;
        }
        else{
            return right;
        }
    }
}

int FindKthState(vector<int>& arr, int k){

    int pivot;
    int pivotIndex;

    int left = 0;
    int right = arr.size() - 1;

    while(left < right){
        pivotIndex = FindPivotIndex(arr, left, right);
        pivot = arr[pivotIndex];
        swap(arr[pivotIndex], arr[left]);
        int i = right;
        int j = right;

        for(; j > left; --j){
            if(arr[j] > pivot){
                swap(arr[i], arr[j]);
                --i;
            }
        }
        swap(arr[i], arr[left]);
        if(i == k){
            return pivot;
        }
        else{
            if(i > k){
                right = i - 1;
            }
            else{
                left = i + 1;
            }
        }
    }

    return arr[left];
}

int main(){
    vector<int> arr;
    int n;
    int k;
    int element;
    cin >> n >> k;

    for(int i = 0; i < n; ++i){
        cin >> element;
        arr.push_back(element);
    }

    cout << FindKthState(arr, k);
    return 0;
}
