/*Леон и Матильда собрались пойти в магазин за молоком.
Их хочет поймать Стэнсфилд, поэтому нашим товарищам нужно сделать это как можно быстрее.
Каково минимальное количество улиц, по которым пройдёт хотя бы один из ребят (либо Матильда, либо Леон, либо оба вместе)?*/
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <queue>

using namespace std;

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    ~Graph() = default;
    void AddEdge(size_t first, size_t second);
    size_t size() const;
    const vector<size_t>& GetNeighbours(size_t vertex) const;
private:
    vector<vector<size_t>> edges;
};

void Graph::AddEdge(size_t first, size_t second){
    edges[first].push_back(second);
    edges[second].push_back(first);
}

size_t Graph::size() const{
    return edges.size();
}

const vector<size_t>& Graph::GetNeighbours(size_t vertex) const{
    return edges[vertex];
}

void CalcAllDistances(const Graph& graph, size_t start, vector<size_t>& distances){
    vector<short> colors(graph.size());
    queue<size_t> q;
    q.push(start);
    colors[start] = 1;
    distances[start] = 0;
    while(!q.empty()){
        size_t it = q.front();
        size_t dist = distances[it];
        vector<size_t> neighbours = graph.GetNeighbours(it);
        for(size_t i = 0; i < neighbours.size(); ++i){
            if(colors[neighbours[i]] == 0){
                distances[neighbours[i]] = dist + 1;
                colors[neighbours[i]] = 1;
                q.push(neighbours[i]);
            }
        }
        colors[it] = 2;
        q.pop();
    }
}

size_t CalcPath(const Graph& graph, size_t first, size_t second, size_t destination){
    size_t minimum = 2 * graph.size();
    vector<size_t> distancesToFirst(graph.size());
    vector<size_t> distancesToSecond(graph.size());
    vector<size_t> distancesToDestination(graph.size());
    CalcAllDistances(graph, first, distancesToFirst);
    CalcAllDistances(graph, second, distancesToSecond);
    CalcAllDistances(graph, destination, distancesToDestination);
    for(size_t i = 0; i < graph.size(); ++i){
        if((distancesToFirst[i] + distancesToSecond[i] + distancesToDestination[i]) < minimum){
            minimum = distancesToFirst[i] + distancesToSecond[i] + distancesToDestination[i];
        }
    }
    return minimum;
}

int main(){
    size_t n;
    cin >> n;
    Graph g(n);
    size_t m;
    cin >> m;
    size_t Leon, Matilda, milk;
    cin >> Leon >> Matilda >> milk;
    for(size_t i = 0; i < m; ++i){
        size_t a = 0;
        size_t b = 0;
        cin >> a >> b;
        g.AddEdge(a - 1, b - 1);
    }
    cout << CalcPath(g, Leon - 1, Matilda - 1, milk - 1);
    return 0;
}
