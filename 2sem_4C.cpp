/*У художника-авангардиста есть полоска разноцветного холста.
За один раз он перекрашивает некоторый отрезок полоски в некоторый цвет.
После каждого перекрашивания специально обученный фотограф делает снимок части получившегося творения для музея современного искусства.
Для правильного экспонирования требуется знать яркость самого темного цвета на выбранном фотографом отрезке.
Напишите программу для определения яркости самого темного цвета на отрезке.
Требуемая скорость определения яркости — O(log N).
Цвет задается тремя числами R, G и B (0 ≤ R, G, B ≤ 255), его яркость = R + G + B.
Цвет (R1, G1, B1) темнее цвета (R2, G2, B2), если R1 + G1 + B1 < R2 + G2 + B2.*/
#include <iostream>
#include <vector>
#include <limits>

using namespace std;

class RangeTree{
public:
    RangeTree(const vector<int>& arr);
    int RangeMinimumQuery(size_t left, size_t right);
    void Assign(size_t left, size_t right, int val);
private:
    int InnerRMQ(size_t vertex, size_t left, size_t right, size_t ctrlLeft, size_t ctrlRight);
    void InnerAssign(size_t vertex, size_t left, size_t right, int val, size_t ctrlLeft, size_t ctrlRight);
    void Push(size_t vertex);
    vector<int> add;
    vector<int> tree;
};

RangeTree::RangeTree(const vector<int>& arr): add(arr.size(), -1), tree(arr.size()){
    size_t size = 1;
    while(size < arr.size()){
        size <<= 1;
    }
    while(add.size() < 2 * size - 1){
        add.push_back(-1);
        tree.push_back(0);
    }
    size_t ind = 0;
    for(; ind < arr.size(); ++ind){
        tree[size - 1 + ind] = arr[ind];
    }
    while(size - 1 + ind < tree.size()){
        tree[size - 1 + ind] = numeric_limits<int>::max();
        ind++;
    }
    for(int i = size - 2; i >= 0; --i){
        tree[i] = min(tree[2 * i + 1], tree[2 * i + 2]);
    }
}

int RangeTree::InnerRMQ(size_t vertex, size_t left, size_t right, size_t ctrlLeft, size_t ctrlRight){
    Push(vertex);
    if(left == ctrlLeft && right == ctrlRight){
        return tree[vertex];
    }
    size_t middle = (ctrlLeft + ctrlRight) / 2;
    if(right <= middle){
        return InnerRMQ(2 * vertex + 1, left, right, ctrlLeft, middle);
    }
    if(left > middle){
        return InnerRMQ(2 * vertex + 2, left, right, middle + 1, ctrlRight);
    }
    return min(InnerRMQ(2 * vertex + 1, left, middle, ctrlLeft, middle),
               InnerRMQ(2 * vertex + 2, middle + 1, right, middle + 1, ctrlRight));
}

int RangeTree::RangeMinimumQuery(size_t left, size_t right){
    return InnerRMQ(0, left, right, 0, tree.size() / 2);
}

void RangeTree::InnerAssign(size_t vertex, size_t left, size_t right, int val, size_t ctrlLeft, size_t ctrlRight){
    Push(vertex);
    if(left == ctrlLeft && right == ctrlRight){
        tree[vertex] = val;
        add[vertex] = val;
        return;
    }
    size_t middle = (ctrlLeft + ctrlRight) / 2;
    if(right <= middle){
        InnerAssign(2 * vertex + 1, left, right, val, ctrlLeft, middle);
        tree[vertex] = min(tree[2 * vertex + 1], tree[2 * vertex + 2]);
        return;
    }
    if(left > middle){
        InnerAssign(2 * vertex + 2, left, right, val, middle + 1, ctrlRight);
        tree[vertex] = min(tree[2 * vertex + 1], tree[2 * vertex + 2]);
        return;
    }
    InnerAssign(2 * vertex + 1, left, middle, val, ctrlLeft, middle);
    InnerAssign(2 * vertex + 2, middle + 1, right,  val, middle + 1, ctrlRight);
    tree[vertex] = min(tree[2 * vertex + 1], tree[2 * vertex + 2]);
}

void RangeTree::Push(size_t vertex){
    if(add[vertex] == -1){
        return;
    }
    if(2 * vertex + 2 >= tree.size()){
        add[vertex] = -1;
        return;
    }
    for(size_t i = 1; i < 3; ++i){
        if(tree[2 * vertex + i] == numeric_limits<int>::max()){
            continue;
        }
        tree[2 * vertex + i] = add[vertex];
        add[2 * vertex + i] = add[vertex];
    }
    add[vertex] = -1;
}

void RangeTree::Assign(size_t left, size_t right, int val){
    InnerAssign(0, left, right, val, 0, tree.size() / 2);
}

int main(){
    size_t n;
    cin >> n;
    vector<int> picture;
    for(size_t i = 0; i < n; ++i){
        int r, g, b;
        cin >> r >> g >> b;
        picture.push_back(r + g + b);
    }
    size_t k;
    cin >> k;
    RangeTree tree(picture);
    for(size_t i = 0; i < k; ++i){
        int c, d, r, g, b, e, f;
        cin >> c >> d >> r >> g >> b >> e >> f;
        tree.Assign(c, d, r + g + b);
        cout << tree.RangeMinimumQuery(e, f) << ' ';
    }
    return 0;
}
