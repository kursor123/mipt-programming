#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <assert.h>

using namespace std;

struct Point{
    int coordinate;
    int type;

    Point(int _coordinate, int _type) {
      coordinate = _coordinate;
      type = _type;
    }
};

typedef struct Point Point;

class Heap{
    public:
        Point ExtractMax();
        Heap(vector<Point> arr){
            size = arr.size();
            for(int i = 0; i < size; ++i){
                data.push_back(arr[i]);
                SiftUp(i);
            }
        }
    private:
        vector<Point> data;
        int size;
        void SiftUp(int number);
        void SiftDown(int number);
};

void Heap::SiftUp(int number){
    if (number == 0){
        return;
    }
    else{
        int parent = (number - 1) / 2;
        if(Heap::data[number].coordinate > Heap::data[parent].coordinate){
            swap(Heap::data[number], Heap::data[parent]);
            Heap::SiftUp(parent);
        }
        else{
            return;
        }
    }
}

void Heap::SiftDown(int number){
    int leftChild = 2 * number + 1;

    if(leftChild > Heap::size - 1){
        return;
    }
    else{
        if(leftChild == Heap::size - 1){
            if(Heap::data[number].coordinate >= Heap::data[leftChild].coordinate){
                return;
            }
            swap(Heap::data[number], Heap::data[leftChild]);
        }
        else{
            if(Heap::data[leftChild].coordinate < Heap::data[leftChild + 1].coordinate){
                if(Heap::data[number].coordinate < Heap::data[leftChild + 1].coordinate){
                    swap(Heap::data[number], Heap::data[leftChild + 1]);
                    Heap::SiftDown(leftChild + 1);
                }
                else{
                    return;
                }
            }
            else{
                if(Heap::data[number].coordinate < Heap::data[leftChild].coordinate){
                    swap(Heap::data[number], Heap::data[leftChild]);
                    Heap::SiftDown(leftChild);
                }
                else{
                    return;
                }
            }
        }
    }
}

Point Heap::ExtractMax(){
    Point value = Heap::data[0];
    swap(Heap::data[0], Heap::data[size - 1]);
    Heap::size--;
    Heap::SiftDown(0);
    Heap::data.pop_back();
    return value;

}

void HeapSort(vector<Point>& arr){

    Heap heap(arr);

    int size = arr.size();

    for(int i = size - 1; i >= 0; --i){
        arr[i] = heap.ExtractMax();
    }
}

int CountLength(vector<Point>& arr){
    int length = 0;

    int currState = 0;

    int arrayLength = arr.size();

    for(int i = 0; i < arrayLength; ++i){
        if(currState == 1){
            length+= (arr[i].coordinate - arr[i - 1].coordinate);
        }
        if(arr[i].type == 1){
            currState--;
        }
        else{
            currState++;
        }
    }
    return length;
}

int main(){

    vector<Point> arr;

    int n = 0;
    int left = 0;
    int right = 0;

    cin >> n;

    for(int i = 0; i < n; ++i){
        cin >> left >> right;

        arr.push_back(Point(left, 0));
        arr.push_back(Point(right, 1));
    }

    HeapSort(arr);
    cout << CountLength(arr);

    return 0;
}
