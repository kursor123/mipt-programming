/*Рику необходимо попасть на межвселенную конференцию.
За каждую телепортацию он платит бутылками лимонада,
поэтому хочет потратить их на дорогу как можно меньше
(он же всё-таки на конференцию едет!).
Однако после K перелетов подряд Рика начинает сильно тошнить,
и он ложится спать на день. Ему известны все существующие телепортации.
Теперь Рик хочет найти путь (наименьший по стоимости в бутылках лимонада),
учитывая, что телепортация не занимает времени, а до конференции осталось 10 минут
(то есть он может совершить не более K перелетов)!*/
#include <iostream>
#include <vector>
#include <limits>

using namespace std;

struct Edge{
    Edge(size_t _second, int _cost): second(_second), cost(_cost){}
    size_t second;
    int cost;
};

class Graph{
public:
    explicit Graph(size_t n): edges(n){}
    void AddEdge(size_t first, size_t second, int cost);
    size_t Size() const;
    void GetNeighbours(size_t vertex, vector<Edge>& neighbours) const;
private:
    vector<vector<Edge>> edges;
};

void Graph::AddEdge(size_t first, size_t second, int cost){
    edges[first].emplace_back(second, cost);
}

size_t Graph::Size() const{
    return edges.size();
}

void Graph::GetNeighbours(size_t vertex, vector<Edge>& neighbours) const{
    neighbours = edges[vertex];
}

const int INF = numeric_limits<int>::max();

int FindShortestPath(const Graph& g, size_t start, size_t k, size_t fin){
    vector<vector<int>> dist(g.Size(), vector<int>(k + 1, INF));
    for(size_t i = 0; i < k + 1; ++i){
        dist[start][i] = 0;
    }
    for(size_t i = 1; i < k + 1; ++i){
        for(size_t j = 0; j < g.Size(); ++j){
            vector<Edge> neighbours;
            g.GetNeighbours(j, neighbours);
            for(size_t t = 0; t < neighbours.size(); ++t){
                if (dist[j][i - 1] != INF){
                    dist[neighbours[t].second][i] = min(min(dist[neighbours[t].second][i - 1], dist[neighbours[t].second][i]),
                                                     dist[j][i - 1] + neighbours[t].cost);
                }
            }
        }
    }
    return (dist[fin][k] < INF)?dist[fin][k]:(-1);
}

int main(){
    size_t n, m, k, s, f;
    cin >> n >> m >> k >> s >> f;
    Graph g(m);
    for(size_t i = 0; i < m; ++i){
        size_t si, fi;
        int pi;
        cin >> si >> fi >> pi;
        g.AddEdge(si - 1, fi - 1, pi);
    }
    cout << FindShortestPath(g, s - 1, k, f - 1);
    return 0;
}
