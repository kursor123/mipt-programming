#include <iostream>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <random>
#include <memory.h>

using namespace std;

bool pivotUpgrade = false;
bool partitionUpgrade = false;
bool firstBranchUpgrade = false;
bool secondBranchUpgrade = false;
bool endUpgrade = false;

int FindPivotIndex(vector<int>&arr, int left, int right){
    if(!pivotUpgrade){
        return left;
    }
    else{
        int middle = (left + right) / 2;
        int value = max(max(min(arr[left], arr[right]), min(arr[left], arr[middle])), min(arr[middle], arr[right]));
        if(arr[left] == value){
            return left;
        }
        else{
            if(arr[middle] == value){
                return middle;
            }
            else{
                return right;
            }
        }
    }
}

int Partition(vector<int>& arr, int left, int right){
    if(!partitionUpgrade){
        int pivot;
        int pivotIndex;

        pivotIndex = FindPivotIndex(arr, left, right);
        pivot = arr[pivotIndex];
        swap(arr[pivotIndex], arr[left]);
        int i = right;
        int j = right;
        for(; j > left; --j){
            if(arr[j] > pivot){
                swap(arr[i], arr[j]);
                --i;
            }
        }
        swap(arr[i], arr[left]);

        return i;
    }
    else{
        int pivot;
        int pivotIndex;

        pivotIndex = FindPivotIndex(arr, left, right);
        pivot = arr[pivotIndex];
        swap(arr[pivotIndex], arr[left]);
        int i = left;
        int j = right;
        while(i < j){
            while((i <= right) &&(arr[i] <= pivot)){
                i++;
            }
            while((j >= left)&&(arr[j] > pivot)){
                j--;
            }
            if(i < j){
                swap(arr[i], arr[j]);
            }
        }
        swap(arr[left], arr[j]);
        return j;
    }
}

void InsertionSort(vector<int>& arr, int left, int right){
    for(int j = left + 1; j <= right; ++j){
        for(int i = j; arr[i] < arr[i - 1]; --i){
            swap(arr[i], arr[i - 1]);
            if(i == 1){
                break;
            }
        }
    }
}

void QuickSort(vector<int>& arr, int left, int right){
    if(endUpgrade){
        if(secondBranchUpgrade){
            vector<int> left;
            vector<int> right;
            left.push_back(0);
            right.push_back(arr.size() - 1);
            int leftBorder = left[0];
            int rightBorder = right[0];
            int border;
            while(left.size() > 0){
                leftBorder = left[left.size() - 1];
                rightBorder = right[right.size() - 1];
                left.pop_back();
                right.pop_back();
                if(rightBorder - leftBorder < 23){
                    InsertionSort(arr, leftBorder, rightBorder);
                }
                else{
                    while(leftBorder < rightBorder){
                        border = Partition(arr, leftBorder, rightBorder);
                        if(border == leftBorder){
                            leftBorder++;
                        }
                        else{
                            left.push_back(leftBorder);
                            right.push_back(border - 1);
                            leftBorder = border + 1;
                        }
                    }
                }
            }
        }
        else{
            if(firstBranchUpgrade){
                int border;
                while(right - left > 23){
                    border = Partition(arr, left, right);
                    QuickSort(arr, left, border - 1);
                    left = border + 1;
                }
                InsertionSort(arr, left, right);
            }
            else{
                if(right - left < 23){
                    InsertionSort(arr, left, right);
                }
                else{
                    int border = Partition(arr, left, right);
                    if(border == right){
                        QuickSort(arr, left, border - 1);
                    }
                    else{
                        QuickSort(arr, left, border);
                        QuickSort(arr, border + 1, right);
                    }
                }
            }
        }
    }
    else{
        if(secondBranchUpgrade){
            vector<int> left;
            vector<int> right;
            left.push_back(0);
            right.push_back(arr.size() - 1);
            int leftBorder = left[0];
            int rightBorder = right[0];
            int border;
            while(left.size() > 0){
                leftBorder = left[left.size() - 1];
                rightBorder = right[right.size() - 1];
                left.pop_back();
                right.pop_back();
                while(leftBorder < rightBorder){
                    border = Partition(arr, leftBorder, rightBorder);
                    if(border == leftBorder){
                        leftBorder++;
                    }
                    else{
                        left.push_back(leftBorder);
                        right.push_back(border - 1);
                        leftBorder = border + 1;
                    }
                }
            }
        }
        else{
            if(firstBranchUpgrade){
                int border;
                while(left < right){
                    border = Partition(arr, left, right);
                    QuickSort(arr, left, border - 1);
                    left = border + 1;
                }
            }
            else{
                if(left == right){
                    return;
                }
                int border = Partition(arr, left, right);
                if(border == right){
                    QuickSort(arr, left, border - 1);
                }
                else{
                    QuickSort(arr, left, border);
                    QuickSort(arr, border + 1, right);
                }
            }
        }
    }
}

int main(int argc, char** argv){
    int n;
    n = 1000000;
    std::mt19937 mt(6);
    std::uniform_int_distribution<int> dist(0, 4294967295);
    for(int i = 1; i < argc; ++i){
        if(argv[i][0] == 'p' && argv[i][1] == 'i'){
            pivotUpgrade = true;
        }
        if(argv[i][0] == 'p' && argv[i][1] == 'a'){
            partitionUpgrade = true;
        }
        if(argv[i][0] == '1'){
            firstBranchUpgrade = true;
        }
        if(argv[i][0] == '2'){
            secondBranchUpgrade = true;
        }
        if(argv[i][0] == 'e'){
            endUpgrade = true;
        }
    }
    vector<int> arr;
    int element;
    for(int i = 0; i < n; ++i){
        arr.push_back(dist(mt));
    }
    QuickSort(arr, 0, n - 1);
    return 0;
}
