/*Джон Макклейн сообщает по рации новую информацию о террористах в отдел с n полицейскими.
Он звонит нескольким сотрудникам и просит распространить информацию по отделу, зная, что у каждого полицейского есть связь с определёнными коллегами.
Джон Макклейн хочет, чтобы операция прошла успешно.
Но если полицейский позвонит коллеге, от которого(возможно, не напрямую) сам получил информацию,
террористы смогут отследить этот звонок и помешать операции.
Если же двое сотрудников оповестят одного, ничего плохого не произойдёт.
Помогите Джону Макклейну.
Выведите NO, если кто-то из полицейских ошибётся, делая звонок.
Если всё пройдёт хорошо, выведите YES и порядок, в котором полицейские получат информацию, считая,
что полицейские оповещают коллег по возрастанию их номеров, а в начале Джон даёт информацию тем, кому не может позвонить никто из коллег.*/
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <queue>
#include <stack>
#include <assert.h>

using namespace std;

class Graph{
public:
    explicit Graph(size_t n): out(n){}
    ~Graph() = default;
    void AddEdge(size_t first, size_t second);
    const vector<size_t>& GetNeighbours(size_t vertex) const;
    size_t size() const;
private:
    vector<vector<size_t>> out;
};

void Graph::AddEdge(size_t first, size_t second){
    out[first].push_back(second);
}

const vector<size_t>& Graph::GetNeighbours(size_t vertex) const{
    return out[vertex];
}

size_t Graph::size() const{
    return out.size();
}

bool AddToWalk(const Graph& graph, size_t vertex, vector<size_t>& walk, vector<size_t>& colors){
    colors[vertex] = 1;
    const vector<size_t>& neighbours = graph.GetNeighbours(vertex);
    for(int i = 0; i < neighbours.size(); ++i){
        if(colors[neighbours[i]] == 1 ||(colors[neighbours[i]] == 0 && AddToWalk(graph, neighbours[i], walk, colors))){
            return true;
        }
    }
    walk.push_back(vertex);
    colors[vertex] = 2;
    return false;
}

bool HasCycles(const Graph& graph, vector<size_t>& walk){
    vector<size_t> colors(graph.size());
    size_t walkIt = 0;
    while(walkIt < graph.size()){
        while(colors[walkIt] != 0){
            walkIt++;
        }
        if(walkIt < graph.size()){
            if(AddToWalk(graph, walkIt, walk, colors)){
                return true;
            }
            walkIt++;
        }
    }
    return false;
}

int main(){
    size_t n;
    cin >> n;
    Graph g(n);
    size_t m;
    cin >> m;
    for(size_t i = 0; i < m; ++i){
        size_t a = 0;
        size_t b = 0;
        cin >> a >> b;
        if(a == b){
            cout << "NO";
            return 0;
        }
        g.AddEdge(a, b);
    }
    vector<size_t> seq;
    if(HasCycles(g, seq)){
        cout << "NO";
    }
    else{
        cout << "YES" << endl;
        for(int i = 0; i < seq.size(); ++i){
            cout << seq[seq.size() - i - 1] << ' ';
        }
    }
    return 0;
}
